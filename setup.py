#!/usr/bin env python
# -*- coding: utf-8 -*-

#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              KSVD 
#  European Synchrotron Radiation Facility, Grenoble,France
#
# KSVD is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# KSVD is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# KSVD; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# KSVD follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



"""
Installation script: 

python setup.py build

"""

__author__ = "Jerome Kieffer"
__contact__ = "Jerome.Kieffer@ESRF.eu"
__license__ = "GPLv2+"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"
__date__ = "28/03/2013"
__satus__ = "development"
import sys, os
import glob
from distutils.core import Extension, setup
from distutils.command.build_ext import build_ext
from os.path import join as pjoin
import string
import stat 

try:
    import numpy
except ImportError:
    text = "You must have numpy installed.\n"
    text += "See http://sourceforge.net/project/showfiles.php?group_id=1369&package_id=175103\n"
    raise ImportError, text

try:
    from Cython.Distutils import build_ext
    CYTHON = True
except ImportError:
    CYTHON = False


if CYTHON:
    cython_c_ext = ".pyx"
else:
    cython_c_ext = ".c"
    from distutils.command.build_ext import build_ext



version = [eval(l.split("=")[1]) for l in open(os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "ksvd", "__init__.py"))
    if l.strip().startswith("version")][0]



jn = os.sep.join



cmdclass = {}
ext_modules = []
internal_clibraries = []
packages = ['PyHST'+version]
package_dir = {'PyHST'+version: 'PyHST'}
if sys.platform == "win32":
    raise RuntimeError("non sense ")
else:
    define_macros = []
    script_files = glob.glob('scripts/*')


def find_in_path(name, path):
    "Find a file in a search path"
    #adapted fom http://code.activestate.com/recipes/52224-find-a-file-given-a-search-path/
    for dirn in path.split(os.pathsep):
        binpath = pjoin(dirn, name)
        if os.path.exists(binpath):
            return os.path.abspath(binpath)
    return None


def locate_cuda():
    """Locate the CUDA environment on the system

    Returns a dict with keys 'home', 'nvcc', 'include', and 'lib'
    and values giving the absolute path to each directory.

    Starts by looking for the CUDAHOME env variable. If not found, everything
    is based on finding 'nvcc' in the PATH.
    """

    # first check if the CUDAHOME env variable is in use
    if 'CUDAHOME' in os.environ:
        home = os.environ['CUDAHOME']
        nvcc = pjoin(home, 'bin', 'nvcc')
    else:
        # otherwise, search the PATH for NVCC
        nvcc = find_in_path('nvcc', os.environ['PATH'])
        if nvcc is None:
            raise EnvironmentError('The nvcc binary could not be '
                'located in your $PATH. Either add it to your path, or set $CUDAHOME')
        home = os.path.dirname(os.path.dirname(nvcc))
   
    cudaconfig = {'home':home, 'nvcc':nvcc,
                  'include': pjoin(home, 'include')}
    lib =  pjoin(home, 'lib')
    if os.path.exists(lib+"64") and sys.maxint == 2**63-1:
        cudaconfig["lib"] = lib+"64"
    elif os.path.exists(lib+"32") and sys.maxint == 2**31-1:
        cudaconfig["lib"] = lib+"32"
    elif os.path.exists(lib):
        cudaconfig["lib"] = lib
    else:
        print("No cuda library found !!!!")
    for key, val in cudaconfig.iteritems():
        if not os.path.exists(val):
            raise EnvironmentError('The CUDA %s path could not be located in %s' % (key, val))
    return cudaconfig


def customize_compiler_for_nvcc(self):
    """inject deep into distutils to customize how the dispatch
    to gcc/nvcc works.

    If you subclass UnixCCompiler, it's not trivial to get your subclass
    injected in, and still have the right customizations (i.e.
    distutils.sysconfig.customize_compiler) run on it. So instead of going
    the OO route, I have this. Note, it's kindof like a wierd functional
    subclassing going on."""

    # tell the compiler it can processes .cu
    self.src_extensions.append('.cu')

    # save references to the default compiler_so and _comple methods
    default_compiler_so = self.compiler_so
    super_comp = self._compile

    # now redefine the _compile method. This gets executed for each
    # object but distutils doesn't have the ability to change compilers
    # based on source extension: we add it.
    def _compile(obj, src, ext, cc_args, extra_postargs, pp_opts):
        print src
        print extra_postargs
        if os.path.splitext(src)[1] == '.cu':
            # use the cuda for .cu files
            self.set_executable('compiler_so', CUDA['nvcc'])
            # use only a subset of the extra_postargs, which are 1-1 translated
            # from the extra_compile_args in the Extension class
            postargs = extra_postargs['nvcc']
        else:
            postargs = extra_postargs['gcc']

        super_comp(obj, src, ext, cc_args, postargs, pp_opts)
        # reset the default compiler_so, which we might have changed for cuda
        self.compiler_so = default_compiler_so

    # inject our redefined _compile method into the class
    self._compile = _compile

# run the customize_compiler
class custom_build_ext(build_ext):
    """
    Cuda aware builder with nvcc compiler support
    """
    def build_extensions(self):
        customize_compiler_for_nvcc(self.compiler)
        build_ext.build_extensions(self)


CUDA = locate_cuda()
CUDA["arch"] = ["-gencode" , "arch=compute_13,code=sm_13", 
                "-gencode", "arch=compute_20,code=sm_20",
                 "-gencode", "arch=compute_20,code=sm_21",
                #  "-gencode", "arch=compute_30,code=sm_30",
                "-gencode", "arch=compute_20,code=compute_20" ]
CUDA["Xptxas"] = [" ", " -Xptxas=-v"]


def build_GPU_patches(ext_modules): 

    sorgenti = [ jn(["ksvd","pytv_denoising_patches"+cython_c_ext]),
                                 jn(["ksvd","tv_denoising_patches.cu"])            ]
    module = Extension(jn(["tv_denoising_patches"]),
                       sources = sorgenti,
                       library_dirs=[CUDA['lib']],
                       libraries=['cudart', "cublas", "cuda", "cufft"],
                       runtime_library_dirs=[CUDA['lib']],
                       extra_compile_args={'gcc': ["-fPIC", "-O3"],
                                           'nvcc': CUDA["arch"] + [ "--compiler-options", "-fPIC", "-O3" ]},
                       include_dirs=[numpy.get_include(), CUDA['include'], 'PyHST/Cspace'])
    ext_modules.append(module)




def build_c_ksvd(ext_modules): 
    import os.path as pth
    import glob

    sorgenti = [ jn(["ksvd",'py_c_ksvd'+cython_c_ext]), jn(["ksvd","c_ksvd.c"])]
    module  =      Extension('c_ksvd',
                             sources = sorgenti,
                             libraries=["lapack","blas" ],               # "atlas"
                             include_dirs  = [numpy.get_include()],
                             library_dirs  = ['./'],
                             extra_compile_args={'gcc': ["-fPIC", "-O3"]},
                             )
    
    ext_modules.append(module)

ext_modules=[]
internal_clibraries=[]

build_GPU_patches(ext_modules)
build_c_ksvd(ext_modules)


script_files = glob.glob( './scripts/*')

# We subclass the build_ext class in order to handle compiler flags
# for openmp and opencl etc in a cross platform way
translator = {
        # Compiler
            # name, compileflag, linkflag
        'msvc' : {
            'openmp' : ('/openmp', ' '),
            'debug'  : ('/Zi', ' '),
            'OpenCL' : 'OpenCL',
            },
        'mingw32':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            },
        'default':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            }
        }

cmdclass = {}

class build_ext_ksvd(build_ext):
    def build_extensions(self):
        if self.compiler.compiler_type in translator:
            trans = translator[self.compiler.compiler_type]
        else:
            trans = translator['default']

        for e in self.extensions:
            e.extra_compile_args = [ trans[a][0] if a in trans else a
                                    for a in e.extra_compile_args]
            e.extra_link_args = [ trans[a][1] if a in trans else a
                                 for a in e.extra_link_args]

            if    e.libraries !=[ "fftw3f"] and e.libraries !=[ "mpi_cxx"]:
                e.libraries = filter(None, [ trans[a] if a in trans else None
                                             for a in e.libraries])


            # If you are confused look here:
            # print e, e.libraries
            # print e.extra_compile_args
            # print e.extra_link_args
        build_ext.build_extensions(self)

cmdclass['build_ext'] = build_ext_ksvd

try:
    import sphinx
    import sphinx.util.console
    sphinx.util.console.color_terminal = lambda: False
    from sphinx.setup_command import BuildDoc
except ImportError:
    sphinx = None

if sphinx:
    class build_doc(BuildDoc):

        def run(self):

            # make sure the python path is pointing to the newly built
            # code so that the documentation is built on this and not a
            # previously installed version

            build = self.get_finalized_command('build')
            sys.path.insert(0, os.path.abspath(build.build_lib))

            # Build the Users Guide in HTML and TeX format
            for builder in ('html', 'latex'):
                self.builder = builder
                self.builder_target_dir = os.path.join(self.build_dir, builder)
                self.mkpath(self.builder_target_dir)
                builder_index = 'index_{0}.txt'.format(builder)
                BuildDoc.run(self)
            sys.path.pop(0)
    cmdclass['build_doc'] = build_doc


from distutils.command.install_data import install_data

class smart_install_data(install_data):
    def run(self):
        install_cmd = self.get_finalized_command('install')
        self.install_dir = os.path.join(getattr(install_cmd, 'install_lib'),"EDRSR")
        print "DATA to be installed in %s" %  self.install_dir
        return install_data.run(self)

cmdclass['install_data'] = smart_install_data

cmdclass['build_ext'] = custom_build_ext

setup(name='ksvd',
      version=version,
      author="Alessandro Mirone",
      author_email="mirone@esrf.fr",
      description='descrizione da farsi qui',
      url="",
      download_url="",
      ext_package="ksvd",
      scripts=script_files,
      # ext_modules=[Extension(**dico) for dico in ext_modules],
      packages=["ksvd"],
      package_dir={"ksvd":"ksvd" },
      test_suite="test",
      cmdclass=cmdclass,
      data_files=[],
      ext_modules=ext_modules
      )


