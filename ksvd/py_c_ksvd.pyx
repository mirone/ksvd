import cython
import numpy
cimport numpy
import math
 
#### soit on fait comme ca et la partie cdef extern va dans un pxd homonime
#from tv_denoising_gpu  cimport tv_denoising_fista as c_tv_denoising_fista  ## attention aux noms!!
##### ou bien comme ca, en ligne. Dans les deux ca sil faut le .h

n_comps = len
cdef extern from "c_ksvd.h":
  cdef int c_dictionary_learning(int ,
                                 int ,
                                 float *,
                                 int    ,
                                 float * ,
                                 int,
                                 int )  nogil
  


@cython.boundscheck(False)
def dictionary_learning(numpy.ndarray input_img not None,
                        int n_comps,
                        int n_pursuit,
                        int niters ):
    """
    """ 
   
     
    assert input_img.ndim in [4,5]
    cdef int size_patch, npatches
    #, n_res_comps

    npatches = input_img.shape[0]
    size_patch = numpy.prod(numpy.array(input_img).shape[1:])
    
    cdef  numpy.ndarray[numpy.float32_t, ndim=1] cinput  = numpy.array(numpy.array(input_img, dtype=numpy.float32 ).ravel())
    # cdef  float[:] cinput  = numpy.ascontiguousarray(input_img.ravel(),dtype=numpy.float32)
    cdef  numpy.ndarray[numpy.float32_t, ndim=2] coutput = numpy.zeros([ n_comps, size_patch   ], dtype=numpy.float32)

    
    with nogil:
        # n_res_comps = 
        c_dictionary_learning(npatches,size_patch  ,  &cinput[0], n_comps,  &coutput[0,0] , n_pursuit ,niters )

    return coutput
                   
