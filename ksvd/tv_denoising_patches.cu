#include <stdio.h>
#include <stdlib.h>
#include<math.h>
#include <cuda.h>
#include <cublas.h>
#include <assert.h>

#  define CUDACHECK \
  {\
    cudaError_t last = cudaGetLastError();\
    if(last!=cudaSuccess) {\
      printf("ERRORX: %s  %s  %i \n", cudaGetErrorString( last),    __FILE__, __LINE__    );	\
      exit(1);\
    }\
  }


#  define CUDA_SAFE_CALL_NO_SYNC( call) {                                    \
    cudaError err = call;                                                    \
    if( cudaSuccess != err) {                                                \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
                __FILE__, __LINE__, cudaGetErrorString( err) );              \
        exit(EXIT_FAILURE);                                                  \
    } }

#  define CUDA_SAFE_CALL( call)     CUDA_SAFE_CALL_NO_SYNC(call);                                            \

#include <cufft.h>
#define CUDA_SAFE_FFT(call){                                                   \
    cufftResult err = call;                                                    \
    if( CUFFT_SUCCESS != err) {                                                \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",          \
                __FILE__, __LINE__, err );                                     \
        exit(EXIT_FAILURE);                                                    \
    } }

#define CUFFT_SAFE_CALL(call, extramsg) {do {	\
       cufftResult err = call;    \
       if( CUFFT_SUCCESS  != err) {   \
	 if(err==   CUFFT_INVALID_PLAN   )				\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_INVALID_PLAN"    ,  extramsg); \
	 if(err==  CUFFT_ALLOC_FAILED   )				\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_ALLOC_FAILED"    ,  extramsg); \
	 if(err==  CUFFT_INVALID_TYPE   )				\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__,  "CUFFT_INVALID_TYPE"  ,  extramsg); \
	 if(err==  CUFFT_INVALID_VALUE  )				\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_INVALID_VALUE"   ,  extramsg); \
	 if(err==    CUFFT_INTERNAL_ERROR)				\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_INTERNAL_ERROR"   ,  extramsg); \
	 if(err==  CUFFT_EXEC_FAILED )					\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_EXEC_FAILED"     ,  extramsg); \
	 if(err==    CUFFT_SETUP_FAILED )				\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_SETUP_FAILED"   ,  extramsg); \
	 if(err==  CUFFT_INVALID_SIZE)					\
	   sprintf(messaggio, "Error in file '%s' in line %i :%s -- %s \n", \
                   __FILE__, __LINE__, "CUFFT_INVALID_SIZE"   ,  extramsg); \
	 fprintf(stderr, messaggio );					\
	 exit(EXIT_FAILURE);						\
	 								\
       } } while (0) ;}


//Round a / b to nearest higher integer value
int iDivUp(int a, int b){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}
//Align a to nearest higher multiple of b
int iAlignUp(int a, int b){
    return (a % b != 0) ?  (a - a % b + b) : a;
}



template<int n_pursuit>
__global__  static  void  kern_omp( float * d_sc, float *d_cc, int nwidget, int pitchpatch, float *d_w, float *d_ss, float tol) {
  int gid;
  gid = threadIdx.x + blockIdx.x*blockDim.x;
  float v[n_pursuit][n_pursuit], scal, coeffs[n_pursuit];
  int  i,j,k,l;
  v[0][0]=1.0f;
  
  if(gid<nwidget) {
    for(k=0; k<n_pursuit; k++) {
      coeffs[k]=0.0f;
    }
    for(i=1; i<n_pursuit; i++) {
      if( fabs(d_sc[i*pitchpatch+gid]) > tol* d_ss[gid]) { 
	for(j=0; j<n_pursuit; j++) {
	  v[i][j]=0.0f;
	}
	v[i][i]=1.0;
	for(j=0; j<i; j++) {
	  scal=0.0f;
	  for(k=0; k<=j; k++) {
	    for(l=0; l<=i; l++) {	    
	      scal += v[i][l] *v[j][k] *   d_cc[ (k*n_pursuit+l)*pitchpatch+gid ] ; 
	    }
	  }
	  for(k=0; k<=j; k++) {
	    v[i][k] -=  scal *v[j][k] ; 
	  }
	  scal=0.0f;
	  for(k=0; k<=i; k++) {
	    for(l=0; l<=i; l++) {	    
	      scal += v[i][l] *v[i][k] *   d_cc[ (k*n_pursuit+l)*pitchpatch+gid ] ; 
	    }
	  }
	  scal=1.0/sqrt(scal);
	  for(k=0; k<=i; k++) {
	    v[i][k] =  scal *v[i][k] ; 
	  }
	}
      }
    } 
    for(i=0; i<n_pursuit; i++) {
      if( fabs(d_sc[i*pitchpatch+gid]) > tol* d_ss[gid]) { 
	scal=0.0f;
	for(k=0; k<=i; k++) {
	  scal+= v[i][k] * d_sc[k*pitchpatch+gid]; 
	}
	for(k=0; k<=i; k++) {
	  coeffs[k]+= v[i][k] * scal; 
	}
      }
    }
    for(k=0; k<n_pursuit; k++)  d_w[k*pitchpatch+gid] =  coeffs[k] ; 
    // for(k=0; k<n_pursuit; k++)  d_w[k*pitchpatch+gid] =  1.0f ; 
  }
}



 
extern "C" {
#include"tv_denoising_patches.h"
}







// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``



// __global__  static  void   kern_set( float *d_w , float value, int npoints ) {
//   int gid;

//   gid = threadIdx.x + blockIdx.x*blockDim.x;
//   if(gid <npoints ) {
//     d_w[gid] = value*(gid%13);
//   }
// }



class Patches_Geometry1D{
public:
  int dim;
  int wp;
  int wc;
  int np;
  int sc;
  int ll, lu;
  Patches_Geometry1D() {
  }
  Patches_Geometry1D(int dim, int wp, int wc) {
    this->dim=dim;
    this->wp=wp;
    this->wc=wc;
    sc = (wp-wc)/2;
    ll = wp-sc-wc;
    lu = iAlignUp(dim,wc)-sc;
    np = iAlignUp(dim,wc)/wc;
  }

  inline __host__ __device__  int local_p(int i)   const { return (i%wc)+sc;} ;
  inline __host__ __device__    int i_patch(int i) const { return i/wc; } ;
  inline  __host__ __device__ int noverlap(int lp) const {                      
    return 1 + lp/wc +(wp-1-lp)/wc;
  }
  inline  __host__ __device__ int lossoverlap(int gp) const {
    int res=0;
    if(gp<ll) {
      res=  (1+(ll-1-gp)/wc);
    }
    if(gp>=lu) {
      res +=  (1+(gp-lu)/wc);
    }
    return res;
  }
};

class Patches_Geometry2D{
public:
  Patches_Geometry1D   pgy;
  Patches_Geometry1D   pgx;
  int np;
  int sizep2d;
  int doppio;

  Patches_Geometry2D() {
  }		       ;

  Patches_Geometry2D(int dim0,int dim1, int dim_patches, int step_for_patches, int doppio) {
    pgy = Patches_Geometry1D( dim0, dim_patches, step_for_patches);
    pgx = Patches_Geometry1D( dim1, dim_patches, step_for_patches);
    np=pgy.np*pgx.np;
    sizep2d =   dim_patches*dim_patches ; 
    this-> doppio = doppio;
  };
};

__global__  static  void put_patches_onimage_kernel( float *d_image  ,float * d_patches   ,Patches_Geometry2D  pg2d  ) {

  int gidx = threadIdx.x + blockIdx.x*blockDim.x;
  int gidy = threadIdx.y + blockIdx.y*blockDim.y;
  
  if( gidx<pg2d.pgx.dim   &&  gidy<pg2d.pgy.dim) {
    int ipx = pg2d.pgx.i_patch( gidx);
    int ipy = pg2d.pgy.i_patch( gidy) ;

    int ilx = pg2d.pgx.local_p( gidx);
    int ily = pg2d.pgy.local_p( gidy) ;

    int ip =     pg2d.pgx.np*ipy   + ipx ; 
    int il =     pg2d.pgx.wp*ily   + ilx ;

    for(int ilayer=0; ilayer<pg2d.doppio; ilayer++) {
      d_image[ (ilayer*pg2d.pgy.dim  +  gidy)*pg2d.pgx.dim  +   gidx  ]  = d_patches[ (ip*pg2d.doppio + ilayer)*pg2d.sizep2d    +il    ]  ;  
    }
  }
}
__global__  static  void put_image_onpatches_kernel( float *d_image  ,float * d_patches   ,Patches_Geometry2D  pg2d , int todo, int done ) {

  int gidx = threadIdx.x + blockIdx.x*blockDim.x ;//  +done;
  int sizep =   pg2d.doppio * pg2d.sizep2d; 

  // if( gidx < pg2d.np * sizep ) {
  if( gidx < done+todo ) {
    int ip = gidx/sizep ;
    
    int ipx = ip %   pg2d.pgx.np ;
    int ipy = ip /   pg2d.pgx.np ;

    int il = gidx%sizep ;

    int ilx =  il                %  pg2d.pgx.wp ; 
    int ily = (il/pg2d.pgx.wp)   %  pg2d.pgy.wp ; 
    int ilz =  il/pg2d.sizep2d      ;  // (pg2d.pgx.wp*pg2d.pgy.wp)  ; 

    d_patches[ gidx  ]    = d_image[ (ilz*pg2d.pgy.dim  + ipy*pg2d.pgy.wc -pg2d.pgy.sc + ily )*pg2d.pgx.dim  + ipx*pg2d.pgx.wc -pg2d.pgx.sc  +  ilx  ]   ;  
  }
}

__global__  static  void prepare_gradient_onpatches_kernel( float *d_image, float *d_image_sum, float *d_error,
							    float * d_patches, float * d_patches_gradient,
							    Patches_Geometry2D  pg2d , int todo, int done,
							    float peso_overlap) {

  int gidx = threadIdx.x + blockIdx.x*blockDim.x ;//  +done;
  int sizep =   pg2d.doppio * pg2d.sizep2d; 

  // if( gidx < pg2d.np * sizep ) {
  if( gidx < done+todo ) {
    int ip = gidx/sizep ;
    
    int ipx = ip %   pg2d.pgx.np ;
    int ipy = ip /   pg2d.pgx.np ;

    int il = gidx%sizep ;

    int ilx =  il                %  pg2d.pgx.wp ; 
    int ily = (il/pg2d.pgx.wp)   %  pg2d.pgy.wp ; 
    int ilz =  il/pg2d.sizep2d      ;  // (pg2d.pgx.wp*pg2d.pgy.wp)  ; 


    int xx =  + ipx*pg2d.pgx.wc -pg2d.pgx.sc  +  ilx    ;
    int yy =  + ipy*pg2d.pgy.wc -pg2d.pgy.sc + ily ;
    
    float res4grad=0.0f;
    float res=0.0f;

    if( xx>=0 &&  yy>=0 && yy<pg2d.pgy.dim && xx<pg2d.pgx.dim) {
      int pos_image =(ilz*pg2d.pgy.dim + yy )*pg2d.pgx.dim  + xx ;
      
      float tmp;
      
      if( ilx>=pg2d.pgx.sc && ilx<(pg2d.pgx.sc+pg2d.pgx.wc) && ily>=pg2d.pgy.sc && ily<(pg2d.pgy.sc+pg2d.pgy.wc) ) {
	int nov = ( pg2d.pgx.noverlap(ilx)-pg2d.pgx.lossoverlap(xx))*(pg2d.pgy.noverlap(ily)-pg2d.pgy.lossoverlap(yy));;
	tmp = d_error[ pos_image  ];

	res4grad = tmp+(  d_image_sum[pos_image] - d_image[pos_image]*nov )*peso_overlap ; 	
	tmp =  0.0f ;
      } else{
	tmp = (  d_image[pos_image] - d_patches[ gidx  ]  );
	res4grad = tmp * peso_overlap;
      }
      res  =  tmp ; 
    }
    d_patches_gradient[ gidx  ]  = res4grad;
    d_patches[ gidx  ]           =  res ;
  }
}


__global__  static  void put_patches_onimage_andsum_kernel( float *d_image  ,float *d_image_sum ,float * d_patches   ,Patches_Geometry2D  pg2d  ) {

  int gidx = threadIdx.x + blockIdx.x*blockDim.x;
  int gidy = threadIdx.y + blockIdx.y*blockDim.y;
  
  float sum[10];

  if( gidx<pg2d.pgx.dim   &&  gidy<pg2d.pgy.dim) {
    int ipx = pg2d.pgx.i_patch( gidx);
    int ipy = pg2d.pgy.i_patch( gidy) ;
    
    int ilx = pg2d.pgx.local_p( gidx);
    int ily = pg2d.pgy.local_p( gidy) ;
        
    int spx  =  max(   ipx - (pg2d.pgx.wp-1-ilx)/pg2d.pgx.wc ,  0             );
    int epx  =  min(   ipx + ilx / pg2d.pgx.wc +1            ,  pg2d.pgx.np   );
    
    int spy  =  max(   ipy - (pg2d.pgy.wp-1-ily)/pg2d.pgy.wc ,  0             );
    int epy  =  min(   ipy + ily / pg2d.pgy.wc +1            ,  pg2d.pgy.np   );


    // int noverlap=( pg2d.pgx.noverlap(ilx)-pg2d.pgx.lossoverlap(gidx))*(pg2d.pgy.noverlap(ily)-pg2d.pgy.lossoverlap(gidy));

    for(int ilayer=0; ilayer<pg2d.doppio; ilayer++) {  
      sum[ilayer]=0.0f;
    }
    for(int Jpx = spx ;  Jpx < epx ; Jpx++ ) {
      for(int Jpy = spy   ;  Jpy < epy ; Jpy++) {
	
	int Jlx = ilx-(Jpx-ipx)*  pg2d.pgx.wc ;
	int Jly = ily-(Jpy-ipy)*  pg2d.pgy.wc ;
	
	int Jp =     pg2d.pgx.np*Jpy   + Jpx ; 
	int Jl =     pg2d.pgx.wp*Jly   + Jlx ;
	

	for(int ilayer=0; ilayer<pg2d.doppio; ilayer++) {  
	  sum[ilayer]+=  d_patches[ (Jp*pg2d.doppio + ilayer)*pg2d.sizep2d    +Jl    ] ;
	}
      }
    }

    int ip =     pg2d.pgx.np*ipy   + ipx ; 
    int il =     pg2d.pgx.wp*ily   + ilx ;


    for(int ilayer=0; ilayer<pg2d.doppio; ilayer++) {
      d_image    [ (ilayer*pg2d.pgy.dim  +  gidy)*pg2d.pgx.dim  +   gidx  ]  = d_patches[ (ip*pg2d.doppio + ilayer)*pg2d.sizep2d    +il    ]  ;  
      d_image_sum[ (ilayer*pg2d.pgy.dim  +  gidy)*pg2d.pgx.dim  +   gidx  ]  =  sum[ilayer]; // /noverlap ;  
    }
  }
}


__global__  static  void kern_shrink_fista_compact( float *d_w,  float *d_dw, float *d_wold,  float  weight, float Lip, 
						   float *d_notzero, float told,  float tnew , int todo, int done ){
  int gid;
  float w0,w1,tmp,  notzero, wold;

  gid = threadIdx.x + blockIdx.x*blockDim.x+done;

  if( gid < done+todo  ) {

    w0 = d_w[gid];
    tmp = w0+d_dw[gid]/Lip;
 
    w1 = copysignf( max(fabs(tmp)-weight,0.0f) , tmp) ;

    wold = d_wold[gid];
    d_wold[gid]=w1 ; 
    w1=w1 +  ((told-1)/tnew) * (w1-wold) ; 
    d_w [gid] = w1;
    notzero=0.0f;
    if(w1!=0.0f) {
      notzero = 1.0f;
    } 
    d_notzero[gid]=notzero;
  }
}
class pippo{
      public:
      ~pippo() {

      }	       	
};


class Buffers {
  
public:
  float *d_w;
  float *d_dw;
  float *d_wold;
  float *d_notzero;
  float *d_image;
  float *d_image_sum;
  float *d_image_error;
  float *d_image_data;

  float *d_data_error;
  float *d_data;

  float *d_patches;
  float *d_patches_gradient;
  float *d_X;
  float *d_kernel;

  int ncomps;
  int sizepatch;
  Patches_Geometry2D pg2d;
  int terminenoto;
  int imsize ; 
  int dimdata; 

  int applykernel;

  void set_w2zero(float *A) {
    CUDA_SAFE_CALL( cudaMemset( A ,0,    pg2d.np*ncomps*sizeof(float)     ));
  }
  Buffers() {


  };

  void setp20() {
    d_w                = NULL ;
    d_dw               = NULL ;
    d_wold             = NULL ;
    d_notzero          = NULL ;
    d_image            = NULL ;
    d_image_sum        = NULL ;
    d_image_error      = NULL ;
    d_image_data       = NULL ;
    d_data_error       = NULL ;
    d_data             = NULL ;
    d_patches          = NULL ;
    d_patches_gradient = NULL ;
    d_X                = NULL ;  
    d_kernel           = NULL ;
  };

  ~Buffers() {
    float * ptrs[]= {       d_w             , d_dw            , d_wold          , 
    			    d_notzero       , d_image         , d_image_sum     , d_image_error   , d_image_data    ,
    			    d_data_error    , d_data          , d_patches       ,  d_patches_gradient , d_X             ,  
    			    d_kernel         };
    for(int i=0; i<14; i++) {
      if(ptrs[i]) {
    	cudaFree( ptrs[i]) ; 
      }
    }
  }
  

  Buffers(  Patches_Geometry2D & pg2d , int ncomps, float* patches, int dimdata , float *img_data, float *kernel) {

    setp20();
    terminenoto=1;
    imsize =   pg2d.pgy.dim*pg2d.pgx.dim* pg2d.doppio; 

    this->pg2d=pg2d;
    this->ncomps=ncomps;
    sizepatch =  pg2d.doppio* pg2d.sizep2d  ; 
    CUDA_SAFE_CALL( cudaMalloc( &(d_w), pg2d.np*ncomps*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( &(d_dw), pg2d.np*ncomps*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( &(d_wold), pg2d.np*ncomps*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( &(d_notzero), pg2d.np*ncomps*sizeof(float)));

    CUDA_SAFE_CALL( cudaMalloc( &d_image      ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_image_sum  ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_image_error  ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;

    CUDA_SAFE_CALL( cudaMalloc( &d_data_error,  dimdata * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_data      ,  dimdata * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;

    CUDA_SAFE_CALL( cudaMalloc( &d_patches    , pg2d.np * sizepatch  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_patches_gradient , pg2d.np * sizepatch  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_X, ncomps*sizepatch*sizeof(float) ));
   
    CUDA_SAFE_CALL( cudaMemcpy( d_X , patches, ncomps*  sizepatch   *sizeof(float) , cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL( cudaMemcpy( d_data     , img_data   , dimdata  * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)  , cudaMemcpyHostToDevice));  
    this->dimdata=dimdata;
    this->applykernel=1;

    CUDA_SAFE_CALL( cudaMalloc( &d_kernel  ,  dimdata * pg2d.pgy.dim   * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMemcpy(  d_kernel    , kernel   ,  dimdata * pg2d.pgy.dim   * sizeof(float)  , cudaMemcpyHostToDevice));  
  }





  Buffers(  Patches_Geometry2D & pg2d , int ncomps, float* patches, float *img_data) {
    setp20();

    terminenoto=1;
    imsize =   pg2d.pgy.dim*pg2d.pgx.dim* pg2d.doppio; 

    this->pg2d=pg2d;
    this->ncomps=ncomps;
    sizepatch =  pg2d.doppio* pg2d.sizep2d  ; 
    CUDA_SAFE_CALL( cudaMalloc( &(d_w), pg2d.np*ncomps*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( &(d_dw), pg2d.np*ncomps*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( &(d_wold), pg2d.np*ncomps*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( &(d_notzero), pg2d.np*ncomps*sizeof(float)));

    CUDA_SAFE_CALL( cudaMalloc( &d_image      ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_image_sum  ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_image_error,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_image_data ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)    ) )	;

    CUDA_SAFE_CALL( cudaMalloc( &d_patches    , pg2d.np * sizepatch  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_patches_gradient , pg2d.np * sizepatch  * sizeof(float)    ) )	;
    CUDA_SAFE_CALL( cudaMalloc( &d_X, ncomps*sizepatch*sizeof(float) ));
   
    CUDA_SAFE_CALL( cudaMemcpy( d_X , patches, ncomps*  sizepatch   *sizeof(float) , cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL( cudaMemcpy( d_image_data     , img_data   ,  pg2d.pgy.dim * pg2d.pgx.dim *  pg2d.doppio  * sizeof(float)  , cudaMemcpyHostToDevice));  

    
    this->applykernel=0;


  }

  float rinormalizza_grad() {
    float norma =  cublasSnrm2( pg2d.np*ncomps  ,  d_dw   , 1);
    cublasSscal(  pg2d.np*ncomps , 1.0f/norma ,d_dw  , 1) ;
    return norma;
  }

  void copia_dws (float *A, float *B) {
    CUDA_SAFE_CALL( cudaMemcpy( A    ,  B  ,  pg2d.np*ncomps*sizeof(float), cudaMemcpyDeviceToDevice));  
  }

  void  calculate_grad_test() {
    float alpha=1.0f, beta=0.0f;
    
    printf("  IN CALCULATE GRAD \n");
    
    cublasSgemm('N','T',sizepatch , pg2d.np ,ncomps,
		alpha,
		d_X , sizepatch,
		d_w , pg2d.np,
		beta,
		d_patches, pg2d.doppio * pg2d.sizep2d);     
    printf("  DOPO GEMM \n");
 
    CUDACHECK;




    {
      dim3 blk,grd;
      int totpatch = pg2d.np*sizepatch;
      
      blk = dim3( 512 , 1 , 1 );
      int done=0;
      
      while(done<totpatch) {
	int nb =   min(  iDivUp(totpatch-done, 512), 0xFFFF )  ; 
	int todo = min( nb* 512 ,totpatch -done   ) ;
	
	grd = dim3( nb , 1, 1 ); 
	put_image_onpatches_kernel<<<grd,blk>>>( d_image_data  , d_patches   , pg2d  , todo, done );
	done+=todo;
      }
    }

    CUDACHECK;


    if(0){
      dim3 blk,grd;
      
      blk = dim3( 32 , 8 , 1 );
      grd = dim3( iDivUp(pg2d.pgx.dim  ,32) , iDivUp(pg2d.pgy.dim  ,8)  , 1 );	  
      
      put_patches_onimage_kernel<<<grd,blk>>>( d_image  , d_patches   , pg2d  );
      
    }
    if(1){
      dim3 blk,grd;	  
      blk = dim3( 32 , 8 , 1 );
      grd = dim3( iDivUp(pg2d.pgx.dim  ,32) , iDivUp(pg2d.pgy.dim  ,8)  , 1 );	  	  
      put_patches_onimage_andsum_kernel<<<grd,blk>>>( d_image  , d_image_sum, d_patches   , pg2d  );	  
    }

    printf("  fatto ritorno \n");
  }


  float  calculate_grad_error(float peso_overlap) {


    {
      float alpha=1.0f, beta=0.0f;
      cublasSgemm('N','T',sizepatch , pg2d.np ,ncomps,
		  alpha,
		  d_X , sizepatch,
		  d_w , pg2d.np,
		  beta,
		  d_patches,sizepatch );     
      CUDACHECK;
    }

    {
      dim3 blk,grd;	  
      blk = dim3( 32 , 8 , 1 );
      grd = dim3( iDivUp(pg2d.pgx.dim  ,32) , iDivUp(pg2d.pgy.dim  ,8)  , 1 );	  	  
      put_patches_onimage_andsum_kernel<<<grd,blk>>>( d_image  , d_image_sum, d_patches   , pg2d  );	  
    }

    float fidelity = 0.0f;

    if( applykernel==0) {
      if(terminenoto) {
	CUDA_SAFE_CALL( cudaMemcpy( d_image_error,d_image_data,  imsize*sizeof(float),cudaMemcpyDeviceToDevice));  
      } else {
	CUDA_SAFE_CALL( cudaMemset(   d_image_error   , 0,   imsize*sizeof(float)      ));
      }
      {
	float alpha= -1.0f;
	cublasSaxpy ( imsize ,  alpha, d_image, 1,d_image_error , 1);
      }
      fidelity =  cublasSnrm2( imsize  ,  d_image_error    , 1);
      fidelity *= fidelity/2 ;
    } else {
      if(terminenoto) {
	CUDA_SAFE_CALL( cudaMemcpy( d_data_error,d_data, dimdata*pg2d.pgx.dim*pg2d.doppio*sizeof(float),cudaMemcpyDeviceToDevice));  
      } else {
	CUDA_SAFE_CALL( cudaMemset(   d_data_error   , 0,  dimdata*pg2d.pgx.dim*pg2d.doppio*sizeof(float)      ));
      }
      {
	float alpha= -1.0f;
	float beta =  1.0f;  
	cublasSgemm('N','N',  pg2d.pgx.dim , dimdata  ,  pg2d.pgy.dim ,
		    alpha,
		    d_image , pg2d.pgx.dim  ,
		    d_kernel    , pg2d.pgy.dim ,
		    beta,
		    d_data_error , pg2d.pgx.dim );   
      }
      fidelity =  cublasSnrm2(  pg2d.pgx.dim * dimdata  ,  d_data_error    , 1);
      fidelity *= fidelity/2 ;
      {
	float alpha=  1.0f;
	float beta =  0.0f;  
	cublasSgemm('N','T',  pg2d.pgx.dim , pg2d.pgy.dim , dimdata  ,  
		    alpha,
		    d_data_error , pg2d.pgx.dim  ,
		    d_kernel     , pg2d.pgy.dim ,
		    beta,
		    d_image_error , pg2d.pgx.dim );   
      }
    }
    {
      dim3 blk,grd;
      int totpatch = pg2d.np*sizepatch;
      
      blk = dim3( 512 , 1 , 1 );
      int done=0;
      
      while(done<totpatch) {
	int nb =   min(  iDivUp(totpatch-done, 512), 0xFFFF )  ; 
	int todo = min( nb* 512 ,totpatch -done   ) ;
	
	grd = dim3( nb , 1, 1 ); 
	prepare_gradient_onpatches_kernel<<<grd,blk>>>( d_image, d_image_sum, d_image_error,
							d_patches, d_patches_gradient,
							pg2d , todo, done,
							peso_overlap);
	done+=todo;
      }
      float tmp =   cublasSnrm2( totpatch  ,  d_patches    , 1) ;
      fidelity += tmp*tmp*peso_overlap/2.0f ;
    }

    CUDACHECK; 

    {
      float alpha= 1.0f;
      float beta = 0.0f;  
      cublasSgemm('T','N',pg2d.np , ncomps ,  sizepatch ,
		  alpha,
		  d_patches_gradient    , sizepatch ,
		  d_X    , sizepatch  ,
		  beta,
		  d_dw  ,pg2d.np  );   
    }
    
    CUDACHECK; 
    
    return fidelity ; 
  }
};


float  tv_denoising_patches_fit_new(int dimz, int dim0,int dim1,float  *img,float  *imgresult , float weight,
				    float *patches, int ncomps, int dim_patches, float  *imgB,float  *imgresultB ,
				    float rho, int step_for_patches, int maxiter)  {  

  float result = 0.0f ;

  printf(" dimz,  dim0, dim1 %d %d %d   \n" ,   dimz,  dim0, dim1 );

  int vectoriality = 1;
  if(imgB) {
    vectoriality=2;
  }
  int doppio =  vectoriality*dimz  ; 

  Patches_Geometry2D   pg2d(dim0, dim1,    dim_patches,  step_for_patches , doppio   );
  Buffers  buffers(   pg2d , ncomps, patches, img) ;

  buffers.set_w2zero(buffers.d_w );
  buffers.terminenoto=1;
  float error=buffers.calculate_grad_error(rho) ;

  buffers.rinormalizza_grad();
  buffers.copia_dws( buffers.d_w, buffers.d_dw );
  buffers.terminenoto=0;
  float norma=0;
  for(int i=0;i<1000; i++) {
    buffers.calculate_grad_error(rho) ;
    norma=  buffers.rinormalizza_grad(); 
    buffers.copia_dws( buffers.d_w, buffers.d_dw );
    printf(" Lipschitz %e \n",  norma );
  }

  float Lip = norma*1.2; 

  buffers.set_w2zero( buffers.d_w    );
  buffers.set_w2zero( buffers.d_wold );

  buffers.terminenoto=1;

  float t=1.0f;
  for(int ivolta=0; ivolta<  maxiter ; ivolta++) {

    float told = t;
    t=(1.0f+sqrt(1.0f+4.0f*t*t))/2.0f;
    
    float error = buffers.calculate_grad_error(rho) ;
    
    float sparsita  = 0.0f ; 
    float L1        = 0.0f ; 
    {
      dim3 blk,grd;
      int totcomp = pg2d.np*ncomps;
      blk = dim3( 512 , 1 , 1 );
      int done=0;
      
      while(done<totcomp) {
	int nb =   min(  iDivUp(totcomp-done, 512), 0xFFFF )  ; 
	int todo = min( nb* 512 ,totcomp -done   ) ;
	grd = dim3( nb , 1, 1 ); 

	kern_shrink_fista_compact<<<grd,blk>>>(  buffers.d_w,    buffers.d_dw , buffers.d_wold,
						 weight/Lip, Lip,  buffers.d_notzero, told, t, todo, done ); 

	done+=todo;
      }
      sparsita =   cublasSasum(totcomp, buffers.d_notzero, 1 ) /totcomp     ; 
      L1       =   cublasSasum(totcomp, buffers.d_w, 1 )   ; 
    }
    printf(" ITER %d TOTALE %e fidelity %e L1 %e  sparsita %e \n", ivolta,  error+L1*weight, error, L1  , sparsita  );
    result =  error+L1*weight ; 
  } 
  cudaMemcpy( imgresult  ,buffers.d_image,  buffers.imsize*sizeof(float),cudaMemcpyDeviceToHost);
  return result ; 
}


float  tv_denoising_patches_fit_deco(int dimdata, int dim0,int dim1,float  *imgdata,float *kernel, float  *imgresult , float weight,
				    float *patches, int ncomps, int dim_patches, 
				    float rho, int step_for_patches, int maxiter)  {  


  float result = 0.0 ; 
  printf(" dimdata,  dim0, dim1 %d %d %d   \n" ,   dimdata,  dim0, dim1 );


  Patches_Geometry2D   pg2d(dim0, dim1,    dim_patches,  step_for_patches , 1   );


  Buffers  buffers(   pg2d , ncomps, patches, dimdata, imgdata, kernel) ;

  buffers.set_w2zero(buffers.d_w );
  buffers.terminenoto=1;
  float error=buffers.calculate_grad_error(rho) ;

  buffers.rinormalizza_grad();
  buffers.copia_dws( buffers.d_w, buffers.d_dw );
  buffers.terminenoto=0;
  float norma=0;
  for(int i=0;i<1000; i++) {
    buffers.calculate_grad_error(rho) ;
    norma=  buffers.rinormalizza_grad(); 
    buffers.copia_dws( buffers.d_w, buffers.d_dw );
    printf(" Lipschitz %e \n",  norma );
  }

  float Lip = norma*1.2; 

  buffers.set_w2zero( buffers.d_w    );
  buffers.set_w2zero( buffers.d_wold );

  buffers.terminenoto=1;

  float t=1.0f;
  for(int ivolta=0; ivolta<  maxiter ; ivolta++) {

    float told = t;
    t=(1.0f+sqrt(1.0f+4.0f*t*t))/2.0f;
    
    float error = buffers.calculate_grad_error(rho) ;
    
    float sparsita  = 0.0f ; 
    float L1        = 0.0f ; 
    {
      dim3 blk,grd;
      int totcomp = pg2d.np*ncomps;
      blk = dim3( 512 , 1 , 1 );
      int done=0;
      
      while(done<totcomp) {
	int nb =   min(  iDivUp(totcomp-done, 512), 0xFFFF )  ; 
	int todo = min( nb* 512 ,totcomp -done   ) ;
	grd = dim3( nb , 1, 1 ); 

	kern_shrink_fista_compact<<<grd,blk>>>(  buffers.d_w,    buffers.d_dw , buffers.d_wold,
						 weight/Lip, Lip,  buffers.d_notzero, told, t, todo, done ); 

	done+=todo;
      }
      sparsita =   cublasSasum(totcomp, buffers.d_notzero, 1 ) /totcomp     ; 
      L1       =   cublasSasum(totcomp, buffers.d_w, 1 )   ; 
    }
    printf(" ITER %d TOTALE %e fidelity %e L1 %e  sparsita %e \n", ivolta,  error+L1*weight, error, L1  , sparsita  );
    result =  error+L1*weight ; 
  } 
  cudaMemcpy( imgresult  ,buffers.d_image,  buffers.imsize*sizeof(float),cudaMemcpyDeviceToHost);
  return result ; 
}









