#include<math.h>
#include<stdio.h>
#include <stdlib.h> 
#include<string.h>
#include<assert.h>
typedef struct {
  float *comps;
  float *patches;
  int sizepatch;
  int npatches;
  int n_comps;
  int n_pursuit;
  // int dim_patches ; 

  int * active_comps;
  float * coeffs ; 
} Problem;


void riordina(Problem *problem) {
  char buff[problem->sizepatch*sizeof(float)];
  
  float tv_vals[problem->npatches];
  int ic;
  int ipa;
  int ipu;
  float tmp;
  for(ic=0; ic<problem->n_comps; ic++) {
    // tv_vals[i]=  TV( problem->comps + sizepatch*i,  dim_patches    )  ; 
    tv_vals[ic]=0;
    for(ipa=0; ipa<problem->npatches; ipa++) {
      for( ipu=0 ; ipu < problem->n_pursuit ; ipu++ ) {
	if(problem->active_comps[ipa*problem->n_pursuit+ipu]==ic) {
	  tmp = problem->coeffs [ipa*problem->n_pursuit+ipu]   ; 
	  tv_vals[ic]+= tmp*tmp   ; 
	}
      }
    }
  }
  int ipu1, ipu2;
  for(ipu1=problem->n_comps-1 ; ipu1>0; ipu1--) {
    for(ipu2=0 ; ipu2<ipu1 ; ipu2++) {
      if( tv_vals[ipu2]< tv_vals[ipu2+1]) {
	tmp= tv_vals[ipu2+1];
	tv_vals[ipu2+1] =  tv_vals[ipu2];
	tv_vals[ipu2] = tmp;
	memcpy(  buff   , problem->comps +(ipu2+1)*problem->sizepatch     , problem->sizepatch*sizeof(float) );
	memcpy(  problem->comps + (ipu2+1)*problem->sizepatch       , problem->comps + (ipu2)*problem->sizepatch     , problem->sizepatch*sizeof(float) );
	memcpy( problem->comps + (ipu2)*problem->sizepatch        , buff , problem->sizepatch*sizeof(float) );
      }
    }
  }
  for(ipu1=problem->n_comps-1 ; ipu1>=0; ipu1--) {
    printf(" %d %e  \n", ipu1,  tv_vals[ipu1]  );
  }
  

}



typedef struct {
  float *S_svd;
  float *U_svd;
  float *VT_svd;
  float * WORK_svd;
  int  L_WORK_svd;
  int * IWORK_svd;

} Buffers_vari;

//extern "C"{
void sgemm_  ( char *transa, char *transb, int *m, int *n, int *k, 
	       float *alpha, float *a, int *Ida, float *b, int *Idb, 
	       float *beta, float* c, int *Idc );
void saxpy_ (int *n,float *alpha, float *A, int *lda ,float *B, int *ldb);
int  isamax_ ( int *N, float *SX, int *INCX ) ; 


void saxpy_(int *N,float *SA,float *SX,int *INCX,float *SY, int *INCY);

void sgesdd_( char *JOBZ, int *M, int *N, float *A, int *LDA,
	      float *S, float *U, int *LDU, float *VT, int *LDVT,
	      float *WORK, int *LWORK, int *IWORK, int *INFO );

void sgetrf_ (int *m, int *n,
	      float a[], int *lda, int ipiv[], int *info);
void sgetrs_ ( char *TRANS, int *N, int * NRHS, float *A, int *LDA, int *IPIV, 
	       float *B, int * LDB, int *INFO ); 

float sdot_(int* , float *,  int *, float * , int * ) ;
float snrm2_(int* , float *,  int * ) ;
void sscal_(int *N,float *SA,float *SX, int *INCX) ;
// }




void  comp_improove(Problem *problem , float * buffer_patches, float * buffer_svd,int   i_comp ,  Buffers_vari *  buffers_vari  ) {
  int i_p;
  int i;
  int incx=1;
  int n_pursuit = problem->n_pursuit ; 
  
  int n_actives=0;
  int i_active[problem->npatches];
  // int order_active[problem->npatches];
  
  float *ptr ;
  float *ptr_svd;
  float alpha;
  
  float * this_comp =  problem->comps + i_comp* problem->sizepatch  ; 
  
  for( i_p=0; i_p<problem->npatches; i_p++) {
    for(i=0;i<n_pursuit;i++) {
      if( problem->active_comps[i_p*n_pursuit  +i]==i_comp   ) {
	i_active[n_actives]= i_p; 
	// order_active[n_actives]=i;
	
	ptr = buffer_patches +i_p*(problem->sizepatch) ;
	ptr_svd = buffer_svd + n_actives*problem->sizepatch ; 
	
	memcpy(ptr_svd, ptr, (problem->sizepatch)*sizeof(float));
	
	alpha = problem->coeffs[i_p*n_pursuit+i] ; 
	saxpy_( &(problem->sizepatch), &alpha, this_comp ,&incx,ptr_svd, &incx);
	
	buffers_vari->VT_svd[n_actives]= alpha ;
	
	n_actives++;
	break;
      }
    }
  }
  // printf("n_actives %d\n", n_actives);
  if(n_actives) {
    
    int ivolta;
    float alpha=1.0;
    float beta=0.0;
    int uno=1;
    float norma, autov, residuo;
    //for(ivolta=0; ivolta<20; ivolta++) {
    while(1)  {
      memcpy( buffers_vari->VT_svd+n_actives  , buffers_vari->VT_svd  ,n_actives*sizeof(float) );

      sgemm_  ( "N","N",
		&(problem->sizepatch) , &(uno),   &(n_actives), 
		&alpha, 
		buffer_svd , &(problem->sizepatch), 
		buffers_vari->VT_svd, &(n_actives) ,
		&beta , 
		buffers_vari->U_svd  , &(problem->sizepatch)  );
      
      sgemm_  ( "T","N",
		&(n_actives), &(uno),  &(problem->sizepatch) , 
		&alpha, 
		buffer_svd , &(problem->sizepatch), 
		buffers_vari->U_svd  , &(problem->sizepatch),
		&beta , 
		buffers_vari->VT_svd, &(n_actives)  );
      
      autov=snrm2_(   &(n_actives)    ,    buffers_vari->VT_svd    , &uno );
      // printf("%d  %e\n", ivolta,  autov);
      norma=-1.0/autov ; 
      saxpy_ (  &(n_actives), &norma, buffers_vari->VT_svd,  &uno,
		buffers_vari->VT_svd+n_actives, &uno);
      residuo = snrm2_(   &(n_actives)    ,    buffers_vari->VT_svd +n_actives   , &uno );
      // printf("residuo %e\n", residuo);
 
      norma=snrm2_(   &(n_actives)    ,    buffers_vari->VT_svd    , &uno );
      norma=1.0f/norma;
      sscal_(&(n_actives) ,&norma, buffers_vari->VT_svd, &uno); 

      if(residuo<1.0e-6) break;
      
    }
    
    norma=snrm2_(   &(problem->sizepatch),    buffers_vari->U_svd    , &uno );
    norma=1.0f/norma;
    sscal_(&(problem->sizepatch) ,&norma, buffers_vari->U_svd, &uno); 
    
    /* char jobz='S'; */
    /* int info; */
    /* sgesdd_( &jobz, &(problem->sizepatch), &(n_actives), buffer_svd, &(problem->sizepatch), */
    /* 	     buffers_vari->S_svd, */
    /* 	     buffers_vari->U_svd, */
    /* 	     &(problem->sizepatch),  */
    /* 	     buffers_vari->VT_svd, */
    /* 	     &(n_actives),  */
    /* 	     buffers_vari->WORK_svd, */
    /* 	     &( buffers_vari->L_WORK_svd), */
    /* 	     buffers_vari->IWORK_svd, */
    /* 	     &info); */
    
    memcpy(this_comp,  buffers_vari->U_svd, problem->sizepatch *sizeof(float) );
    
    for(i=0;i<n_actives;i++) {
      i_p = i_active[i]; 
      
      ptr = buffer_patches +i_p*(problem->sizepatch) ;
      ptr_svd = buffer_svd + i*problem->sizepatch ; 
      
      alpha =  -  sqrt(autov) *  buffers_vari->VT_svd[i]    ; 
      
      saxpy_( &(problem->sizepatch), &alpha, this_comp ,&incx,ptr_svd, &incx);
      
      memcpy(ptr, ptr_svd, (problem->sizepatch)*sizeof(float));
    }
  }  else {
    printf("trovato un non attivo \n");
  }
}



void   pursuit_round( Problem *problem , int  i_pursuit, float *buffer_scalprods , float *buffer_patches) {
  {
    float alpha = 1.0f ;
    float beta  = 0.0f ;

    sgemm_  ( "T","N",
	      &(problem->n_comps) ,&(problem->npatches),   &(problem->sizepatch), 
	      &alpha, 
	      problem->comps, &(problem->sizepatch), 
	      buffer_patches, &(problem->sizepatch) ,
	      &beta , 
	      buffer_scalprods  , &(problem->n_comps)  );
  }

  {
    int i_p;
    int imax, ic, ic_p;
    int incx=1;
    int n_pursuit = problem->n_pursuit ; 
    float *choosed_comp, *choosed_comp_p;
    float S[(i_pursuit+1)*(i_pursuit+1)], D[i_pursuit+1];

    char trans = 'N';
    int dim =  i_pursuit+1;    
    int nrhs = 1;
    int LDA = dim;
    int LDB = dim;
    int info;
    int ipiv[ dim+1];
    
    float coeff;
   
    for( i_p=0; i_p<problem->npatches; i_p++) {

      float *ptr = buffer_scalprods +i_p*(problem->n_comps) ;
      // metti fuori combattimento quelle gia conosciute

      for(ic=0; ic<problem->n_comps; ic++) {
	if ( ptr[   ic ]==0.0f)  ptr[   ic ]=1.0e-16;
      }
      for(ic=0; ic<i_pursuit; ic++) {
	ptr[  problem->active_comps[  i_p * n_pursuit + ic  ]   ]=0.0f;
      }

      imax =  isamax_(&(problem->n_comps),   ptr    , &incx   )  -  1; // meno uno      
      problem->active_comps[  i_p * n_pursuit + i_pursuit ] =  imax ; 

      float *this_patch   = buffer_patches +  i_p *problem->sizepatch ;    
      float *orig_patch   = problem->patches +  i_p *problem->sizepatch ;    

      if(0) {
	coeff = problem->coeffs[  i_p * n_pursuit +i_pursuit ] = ptr[imax];
	coeff=-coeff;
	saxpy_(&(problem->sizepatch),&coeff, choosed_comp ,&incx ,this_patch,&incx );
      } else {
	for(ic=0; ic<=i_pursuit; ic++) {
	  
	  choosed_comp =  problem->comps  +    problem->active_comps[  i_p * n_pursuit + ic ]  *problem->sizepatch;
	  D[ ic ] =  sdot_( &(problem->sizepatch),  orig_patch   ,   &incx    , choosed_comp   , &incx   )  ; 
	  for(ic_p=0; ic_p<=ic; ic_p++) {
	    choosed_comp_p =  problem->comps  +    problem->active_comps[  i_p * n_pursuit + ic_p ]  *problem->sizepatch;
	    S[ ic*(i_pursuit+1) + ic_p ] = sdot_( &(problem->sizepatch),   choosed_comp_p  ,   &incx    , choosed_comp   , &incx   )  ; 
	    S[ ic_p*(i_pursuit+1) + ic ] = S[ ic*(i_pursuit+1) + ic_p ] ;
	  }
	}
	
	sgetrf_(&dim, &dim, S, &LDA, ipiv, &info);
	if(info) {
	  printf("proble %e %d   , %d %d , %d\n", snrm2_( &(problem->sizepatch) , this_patch, &incx   ),  i_p,
		 problem->active_comps[  i_p * n_pursuit + 0 ]  , 
		 problem->active_comps[  i_p * n_pursuit + 1 ], i_pursuit );
	}
	sgetrs_(&trans, &dim, &nrhs, S, &LDA, ipiv, D , &LDB, &info);
	if(info) {
	  printf("proble \n");
	}
	
	for(ic=0; ic<=i_pursuit; ic++) {
	  problem->coeffs[  i_p * n_pursuit + ic] = D[ ic ]; 
	}
	
	memcpy(this_patch, orig_patch,  problem->sizepatch *sizeof(float)     );
	
	for(ic=0; ic<=i_pursuit; ic++) {
	  choosed_comp =  problem->comps  +    problem->active_comps[  i_p * n_pursuit + ic ]  *problem->sizepatch;
	  coeff=  -  problem->coeffs[  i_p * n_pursuit + ic ] ;
	  saxpy_(&(problem->sizepatch),&coeff, choosed_comp ,&incx ,this_patch,&incx );
	}
      } 
    }
  }
}


void trivial_pursuit_all( Problem * problem , float *  buffer_scalprods , float *  buffer_patches ) {
  int n_pursuit = problem->n_pursuit ; 
  memcpy( buffer_patches , problem->patches ,  problem->npatches  *problem->sizepatch    *sizeof(float)    );
  int i_pursuit=0;
  for(i_pursuit=0; i_pursuit<n_pursuit ; i_pursuit++) {
    pursuit_round( problem , i_pursuit , buffer_scalprods, buffer_patches);

    int incx=1;
    int N =   problem->npatches  *problem->sizepatch   ; 
    float residuo2;
    residuo2 = snrm2_( &N , buffer_patches     ,  &incx );
    printf(" residuo   attuale %e \n",  residuo2*residuo2);
  }
  {
    int incx=1;
    int N =   problem->npatches  *problem->sizepatch   ; 
    float residuo2, residuo1;
    residuo2 = snrm2_( &N , buffer_patches     ,  &incx );
    residuo1 = snrm2_( &N , problem->patches   ,  &incx );
    printf(" residuo iniziale %e  attuale %e \n", residuo1*residuo1, residuo2*residuo2);
  }

}


int c_dictionary_learning(int npatches,
			  int sizepatch,
			  float *patches ,
			  int   n_comps ,
			  float * comps ,
			  int n_pursuit,
			  int passaggi)  {

  // int sizepatch = dim_patches*dim_patches ; 
  { // si sottrae da ogni patch la  sua media
    int ip, ix;
    double sum;
    for(ip=0; ip< npatches; ip++) {
      sum=0.0;
      for(ix=0; ix< sizepatch; ix++) {
	sum += patches[ ip*sizepatch + ix ] ; 
      }
      sum /= sizepatch ; 
      for(ix=0; ix< sizepatch; ix++) {
	patches[ ip*sizepatch + ix ] -= sum; 
      }
    }
  }
  {
    // ma si evita di riscalare
  }
  { // si inizializzano le componenti 
    int ic, ix;
    double sum;
    for(ic=0; ic< n_comps; ic++) {
      sum=0.0;
      for(ix=0; ix< sizepatch; ix++) {
	double tmp;
	// comps[ ic*sizepatch + ix ] = tmp= random()*2.0/(RAND_MAX+1.0)  -1 ;
	comps[ ic*sizepatch + ix ] = tmp=  patches[ ic*sizepatch + ix ] ;
	sum+=tmp*tmp;
      }
      sum=sqrt(sum);
      for(ix=0; ix< sizepatch; ix++)  comps[ ic*sizepatch + ix ] /= sum; 
    }
  }
  
  Problem *problem = (Problem*) malloc(sizeof(Problem));
  {
    problem->patches   = patches ; 
    problem->comps     = comps   ; 
    problem->sizepatch = sizepatch  ;
    // problem->dim_patches = dim_patches ; 
    problem->npatches  = npatches  ;
    problem->n_comps   = n_comps   ;
    problem->active_comps =  (int*) malloc( npatches * n_pursuit *sizeof(int) ); 
    problem->coeffs       =  (float*) malloc( npatches * n_pursuit *sizeof(float) ); 
    problem->n_pursuit = n_pursuit ; 
  }
  
  Buffers_vari * buffers_vari = (Buffers_vari*) malloc(sizeof(Buffers_vari));
  {
    assert(  n_comps < npatches );
    buffers_vari->S_svd =  (float*) malloc( 4*sizepatch * sizeof(float) ) ; 
    buffers_vari->U_svd =   (float*) malloc(4* sizepatch * npatches * sizeof(float) ) ; 
    buffers_vari->VT_svd =  (float*) malloc( 4*sizepatch *npatches  * sizeof(float) ) ; 
 
    buffers_vari->L_WORK_svd = 4* (3*sizepatch+(sizepatch+npatches+4*sizepatch*sizepatch+4*sizepatch))  ; 
    buffers_vari->WORK_svd =  (float*) malloc( buffers_vari->L_WORK_svd * sizeof(float) ) ; 
    buffers_vari->IWORK_svd =  (int*) malloc( 4* 8* sizepatch * sizeof(int) ) ; 
    
  }

  float * buffer_scalprods  = (float *) malloc(  npatches *   n_comps  *sizeof(float)  );
  float * buffer_patches    = (float *) malloc(  npatches *   sizepatch  *sizeof(float)  );
  float * buffer_svd   = (float *) malloc( 4*npatches  *   sizepatch  *sizeof(float)  );

  int i_comp;
  int i_pass;
  for(i_pass=0; i_pass<passaggi; i_pass++) {
      trivial_pursuit_all(  problem ,  buffer_scalprods , buffer_patches);
      // int i_comp = (int)( n_comps *  random()*(2.0/(RAND_MAX+1.0)) );
      
    for(i_comp=0; i_comp< n_comps; i_comp++   ) {
      // printf("improve %d \n", i_comp);
      comp_improove(problem , buffer_patches, buffer_svd,   i_comp ,  buffers_vari  );
    }
    if(i_pass==passaggi-1) {
      trivial_pursuit_all(  problem ,  buffer_scalprods , buffer_patches);
      riordina(problem ) ;
    } 

    /* if(i_pass!=passaggi-1) { */
    /*   comp_improove(problem , buffer_patches, buffer_svd,   7 ,  buffers_vari  ); */
    /* } */
  }
  return npatches;
 }




  






/*     SUBROUTINE SGESDD( JOBZ, M, N, A, LDA, S, U, LDU, VT, LDVT, WORK, */
/*      $                   LWORK, IWORK, INFO ) */
/* * */
/* *  -- LAPACK driver routine (version 3.2.1)                                  -- */
/* *  -- LAPACK is a software package provided by Univ. of Tennessee,    -- */
/* *  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..-- */
/* *     March 2009 */
/* * */
/* *     .. Scalar Arguments .. */
/*       CHARACTER          JOBZ */
/*       INTEGER            INFO, LDA, LDU, LDVT, LWORK, M, N */
/* *     .. */
/* *     .. Array Arguments .. */
/*       INTEGER            IWORK( * ) */
/*       REAL               A( LDA, * ), S( * ), U( LDU, * ), */
/*      $                   VT( LDVT, * ), WORK( * ) */
/* *     .. */
/* * */
/* *  Purpose */
/* *  ======= */
/* * */
/* *  SGESDD computes the singular value decomposition (SVD) of a real */
/* *  M-by-N matrix A, optionally computing the left and right singular */
/* *  vectors.  If singular vectors are desired, it uses a */
/* *  divide-and-conquer algorithm. */
/* * */
/* *  The SVD is written */
/* * */
/* *       A = U * SIGMA * transpose(V) */
/* * */
/* *  where SIGMA is an M-by-N matrix which is zero except for its */
/* *  min(m,n) diagonal elements, U is an M-by-M orthogonal matrix, and */
/* *  V is an N-by-N orthogonal matrix.  The diagonal elements of SIGMA */
/* *  are the singular values of A; they are real and non-negative, and */
/* *  are returned in descending order.  The first min(m,n) columns of */
/* *  U and V are the left and right singular vectors of A. */
/* * */
/* *  Note that the routine returns VT = V**T, not V. */
/* * */
/* *  The divide and conquer algorithm makes very mild assumptions about */
/* *  floating point arithmetic. It will work on machines with a guard */
/* *  digit in add/subtract, or on those binary machines without guard */
/* *  digits which subtract like the Cray X-MP, Cray Y-MP, Cray C-90, or */
/* *  Cray-2. It could conceivably fail on hexadecimal or decimal machines */
/* *  without guard digits, but we know of none. */
/* * */
/* *  Arguments */
/* *  ========= */
/* * */
/* *  JOBZ    (input) CHARACTER*1 */
/* *          Specifies options for computing all or part of the matrix U: */
/* *          = 'A':  all M columns of U and all N rows of V**T are */
/* *                  returned in the arrays U and VT; */
/* *          = 'S':  the first min(M,N) columns of U and the first */
/* *                  min(M,N) rows of V**T are returned in the arrays U */
/* *                  and VT; */
/* *          = 'O':  If M >= N, the first N columns of U are overwritten */
/* *                  on the array A and all rows of V**T are returned in */
/* *                  the array VT; */
/* *                  otherwise, all columns of U are returned in the */
/* *                  array U and the first M rows of V**T are overwritten */
/* *                  in the array A; */
/* *          = 'N':  no columns of U or rows of V**T are computed. */
/* * */
/* *  M       (input) INTEGER */
/* *          The number of rows of the input matrix A.  M >= 0. */
/* * */
/* *  N       (input) INTEGER */
/* *          The number of columns of the input matrix A.  N >= 0. */
/* * */
/* *  A       (input/output) REAL array, dimension (LDA,N) */
/* *          On entry, the M-by-N matrix A. */
/* *          On exit, */
/* *          if JOBZ = 'O',  A is overwritten with the first N columns */
/* *                          of U (the left singular vectors, stored */
/* *                          columnwise) if M >= N; */
/* *                          A is overwritten with the first M rows */
/* *                          of V**T (the right singular vectors, stored */
/* *                          rowwise) otherwise. */
/* *          if JOBZ .ne. 'O', the contents of A are destroyed. */
/* * */
/* *  LDA     (input) INTEGER */
/* *          The leading dimension of the array A.  LDA >= max(1,M). */
/* * */
/* *  S       (output) REAL array, dimension (min(M,N)) */
/* *          The singular values of A, sorted so that S(i) >= S(i+1). */
/* * */
/* *  U       (output) REAL array, dimension (LDU,UCOL) */
/* *          UCOL = M if JOBZ = 'A' or JOBZ = 'O' and M < N; */
/* *          UCOL = min(M,N) if JOBZ = 'S'. */
/* *          If JOBZ = 'A' or JOBZ = 'O' and M < N, U contains the M-by-M */
/* *          orthogonal matrix U; */
/* *          if JOBZ = 'S', U contains the first min(M,N) columns of U */
/* *          (the left singular vectors, stored columnwise); */
/* *          if JOBZ = 'O' and M >= N, or JOBZ = 'N', U is not referenced. */
/* * */
/* *  LDU     (input) INTEGER */
/* *          The leading dimension of the array U.  LDU >= 1; if */
/* *          JOBZ = 'S' or 'A' or JOBZ = 'O' and M < N, LDU >= M. */
/* * */
/* *  VT      (output) REAL array, dimension (LDVT,N) */
/* *          If JOBZ = 'A' or JOBZ = 'O' and M >= N, VT contains the */
/* *          N-by-N orthogonal matrix V**T; */
/* *          if JOBZ = 'S', VT contains the first min(M,N) rows of */
/* *          V**T (the right singular vectors, stored rowwise); */
/* *          if JOBZ = 'O' and M < N, or JOBZ = 'N', VT is not referenced. */
/* * */
/* *  LDVT    (input) INTEGER */
/* *          The leading dimension of the array VT.  LDVT >= 1; if */
/* *          JOBZ = 'A' or JOBZ = 'O' and M >= N, LDVT >= N; */
/* *          if JOBZ = 'S', LDVT >= min(M,N). */
/* * */
/* *  WORK    (workspace/output) REAL array, dimension (MAX(1,LWORK)) */
/* *          On exit, if INFO = 0, WORK(1) returns the optimal LWORK; */
/* * */
/* *  LWORK   (input) INTEGER */
/* *          The dimension of the array WORK. LWORK >= 1. */
/* *          If JOBZ = 'N', */
/* *            LWORK >= 3*min(M,N) + max(max(M,N),6*min(M,N)). */
/* *          If JOBZ = 'O', */
/* *            LWORK >= 3*min(M,N) +  */
/* *                     max(max(M,N),5*min(M,N)*min(M,N)+4*min(M,N)). */
/* *          If JOBZ = 'S' or 'A' */
/* *            LWORK >= 3*min(M,N) + */
/* *                     max(max(M,N),4*min(M,N)*min(M,N)+4*min(M,N)). */
/* *          For good performance, LWORK should generally be larger. */
/* *          If LWORK = -1 but other input arguments are legal, WORK(1) */
/* *          returns the optimal LWORK. */
/* * */
/* *  IWORK   (workspace) INTEGER array, dimension (8*min(M,N)) */
/* * */
/* *  INFO    (output) INTEGER */
/* *          = 0:  successful exit. */
/* *          < 0:  if INFO = -i, the i-th argument had an illegal value. */
/* *          > 0:  SBDSDC did not converge, updating process failed. */
/* * */











  
