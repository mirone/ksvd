visu=0

if visu:
    from matplotlib.pyplot import figure, show, axes
    import matplotlib.cm as cm
    import matplotlib.pyplot as plt
    pl=plt

import time
from  math import pi, atan, sqrt
from numpy import array
import numpy
import sys
import string
import h5py


try:
    from scipy.misc import lena
except:
    lena=None
    print " NON LENA ON THE SYSTEM"

from time import time

import numpy as np


import getgpu
gpu=getgpu.getgpu()
try:
    import pycuda.autoinit
    import pycuda.tools
    if gpu!=-1:
        dev=pycuda.driver.Device(gpu)
        CTX=dev.make_context()
    else:
        CTX=pycuda.tools.make_default_context() 
    print "Ma carte est ", pycuda.driver.Context.get_device().name()
except:
    print " NO pycuda "
    # raise " pas de carte et/ou cuda "





import tv_denoising_patches

import h5py

import yaml


def take_roi( image_source, roi  , epaisseur  ):
    if roi is None:
        image = image_source [:]
    else:
        if roi.has_key("slice"):
            image = image_source [ roi["slice"]][:]
        else:
            image = image_source [ roi["slab"]:roi["slab"]+epaisseur ]
            image = image[:,   roi["starty"]:roi["endy"]  ,     roi["startx"]:roi["endx"] ] 

    if len(image.shape)!=3:
        image=[image]
    
    return numpy.array(image,"f")



def main() :
    usage = """  USAGE
       pour lena
        python provapatches.py    filepatches.edf  beta noise
       pour un' image
        python provapatches.py    filepatches.edf  beta noise image.edf
       pour deux images
        python provapatches.py    filepatches.edf  beta noise image.edf  imageB.edf

     se beta est negativo si attiva l' omp
     se noise est negativo si attiva il fit

"""

    inputfile=open(sys.argv[1],"r")
    params  = yaml.load(inputfile)

    beta  = params["beta"]
    rho  = params["rho"]
    noise = params["noise"]
    patches_file = params["patches"]

    imageA_file = params["imageA"]
    
    if params.has_key("imageB"):
        imageB_file = params["imageB"]
    else:
        imageB_file = None
    

    roi = None
    if params.has_key("roi"):
        roi = params["roi"]


    h5 = h5py.File( patches_file ,"r")
    patches = h5["data"][:]
    

    print " SHAPE PATCHES " , patches.shape

    if(len(patches.shape)==4):
        epaisseur = 1
        patches.shape = list(patches.shape[:2])+[1] + list(patches.shape[2:])
    else:
        epaisseur  = patches.shape[2]

    h5 = h5py.File( imageA_file ,"r")
    imageA_source  = h5["data"]

    imageA = take_roi( imageA_source, roi  , epaisseur  )
 

    if imageB_file is not None:

        h5 = h5py.File( imageB_file ,"r")
        imageB_source  = h5["data"]
        
        assert( patches.shape[1]==2  )
        
        imageB = take_roi( imageB_source, roi  , epaisseur   )
    else:
        imageB=None
        

    shift = imageA.min()
    imageA[:] = imageA - shift
    Imax = imageA.max()
    imageA[:] = imageA / Imax +  noise * np.random.normal(size=(imageA.shape))

    if imageB is not None:
        imageB[:] = imageB - shift
        imageB[:] = imageB / Imax +  noise * np.random.normal(size=(imageB.shape))
    

    pp  =  np.ones(patches.shape[1:],"f")
    pp[:] =  numpy.sqrt(1.0/pp.size)

    print pp.shape
    print patches.shape

    patches=numpy.concatenate( ([pp],patches), axis=0)

    resA, resB = tv_denoising_patches.pytv_denoising_patches(imageA, beta, np.array(patches,"f"), imageB, rho, 2, 2000)
    
    f=h5py.File("resA.h5", "w")
    f["data"] = resA
    f.close()
    


    if resB is not None:
        f=h5py.File("resB.h5", "w")
        f["data"]= resB
        f.close()


    from PyMca5.PyMcaIO.EdfFile import EdfFile
    
    f=EdfFile("res.edf","w+")

    resA.shape=[resA.shape[1], resA.shape[2]]
    f.WriteImage({}, resA)

main()
CTX.detach()
