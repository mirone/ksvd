import c_ksvd
import os
import time
from  math import pi, atan, sqrt
from numpy import array
import numpy
from PyMca5.PyMcaIO import EdfFile
# from PyMca import EdfFile
import sys
import string
import scipy.ndimage
import random
import h5py

import matplotlib.pyplot as plt

try:
    from matplotlib.pyplot import figure, show, axes
    import matplotlib.cm as cm
    import matplotlib.pyplot as plt
    pl=plt
    visu=1
except:
    visu=0
    
from time import time

import numpy as np

import yaml


class ImageFile(yaml.YAMLObject) :
    yaml_tag = u'!ImageFile'

    START_X=0
    END_X  =100000
    
    START_Y = 0
    END_Y   = 100000
    
    VAL_MIN=-1.0e+30
    VAL_MAX=+1.0e+30

    FILE_TYPE="edf"
    
    FILE       =  None
    FILE_B     =  None
    DERIVATIVES=0


    N_PATCHES  = 1000

    def volume(self):
        res = self.END_X   -  max(0, self.START_X)
        res*= self.END_Y   -  max(0, self.START_Y)
        return res

    def __repr__(self):
        return "%s(START_X=%r, END_X=%r, START_Y=%r, END_Y=%r, VAL_MIN=%r, VAL_MAX=%r, FILE_TYPE=%r, FILE=%r, FILE_B=%r ,N_PATCHES =%r, DERIVATIVES=%r )" % (
            self.__class__.__name__, self.START_X , self.END_X , self.START_Y , self.END_Y ,
            self.VAL_MIN , self.VAL_MAX , self.FILE_TYPE , 
            self.FILE , self.FILE_B  , self.N_PATCHES,  self.DERIVATIVES)

    def get_patches(self,  patches_container, dims ):

        if self.FILE_TYPE == "edf":
            edf=EdfFile.EdfFile(self.FILE,"r")
            image = edf.GetData(0).astype("f")
        else:
            raise  Exception, " unknown type  " 
        
        if self.FILE_B is not None:
            if self.FILE_TYPE == "edf":
                edf=EdfFile.EdfFile(self.FILE_B)
                imageB = edf.GetData(0).astype("f")
            else:
                raise  Exception, " unknown type  " 
            assert( image.shape_B == image.shape)
        else:
            imageB = None
            if self.DERIVATIVES:
                X     = scipy.ndimage.filters.sobel(image,axis=1 )/8.0
                imageB= scipy.ndimage.filters.sobel(image,axis=0 )/8.0
                image=X

                f=h5py.File(self.FILE+"_X.h5","w")
                f["data"]=image
                f.close()
                f=h5py.File(self.FILE+"_Y.h5","w")
                f["data"]=imageB
                f.close()


        shape = image.shape
        self.END_X = min( self.END_X, shape[1])
        self.END_Y = min( self.END_Y, shape[0])

        n_patches = 0

        while n_patches < self.N_PATCHES : 
            start_y = random.randint(0,self.END_Y - dims[-2])
            start_x = random.randint(0,self.END_X - dims[-1])
            if imageB is None:
                newpatch = numpy.array([image[ start_y:(start_y+dims[-2]) ,  start_x:(start_x+dims[-1])    ] ])
            else:
                newpatch = numpy.array([image [ start_y:(start_y+dims[-2]) ,  start_x:(start_x+dims[-1])],
                                        imageB[ start_y:(start_y+dims[-2]) ,  start_x:(start_x+dims[-1])]] )[:,None,:,:]

            max_p = newpatch.max()
            min_p = newpatch.min()

            if min_p < self.VAL_MAX and  max_p > self.VAL_MIN:
                patches_container[n_patches] =  newpatch
                n_patches += 1

def setchunk(dim):
    res=1
    while res<32 and res<dim:
        res*=2
    if res> dim:
        res=res/2
    return res

class VolumeFile(yaml.YAMLObject) :
    yaml_tag = u'!VolumeFile'

    START_X=0
    END_X  =100000
    
    START_Y = 0
    END_Y   = 100000
    
    START_Z = 0
    END_Z   = 100000
    
    VAL_MIN=-1.0e+30
    VAL_MAX=+1.0e+30

    FILE_TYPE="vol"
    
    FILE       =  None
    FILE_B     =  None
    DERIVATIVES=0

    VDIMX = None
    VDIMY = None
    VDIMZ = None
    
    N_PATCHES  = 1000

    REBIN=1


    def volume(self):
        res = self.END_X   -  max(0, self.START_X)
        res*= self.END_Y   -  max(0, self.START_Y)
        res*= self.END_Z   -  max(0, self.START_Z)
        return res


    def __repr__(self):
        return "%s(START_X=%r, END_X=%r, START_Y=%r, END_Y=%r, START_Z=%r, END_Z=%r, VAL_MIN=%r, VAL_MAX=%r, FILE_TYPE=%r, FILE=%r, FILE_B=%r ,N_PATCHES =%r, DERIVATIVES=%r, VDIMX=%r,  VDIMY=%r,  VDIMZ=%r, REBIN=%r  )" % (
            self.__class__.__name__, self.START_X , self.END_X , self.START_Y , self.END_Y ,self.START_Z , self.END_Z ,
            self.VAL_MIN , self.VAL_MAX , self.FILE_TYPE , 
            self.FILE , self.FILE_B  , self.N_PATCHES,  self.DERIVATIVES,
            self.VDIMX, self.VDIMY,self.VDIMZ, self.REBIN  )

    def get_patches(self,  patches_container, dims ):


        if(len(dims)==2) :
            dims=[1,dims[0], dims[1]]

        self.END_X = min( self.END_X, self.VDIMX )
        self.END_Y = min( self.END_Y, self.VDIMY)
        self.END_Z = min( self.END_Z, self.VDIMZ)

        chunkz, chunky, chunkx =  setchunk(self.VDIMZ), setchunk(self.VDIMY), setchunk(self.VDIMX) 

        if self.FILE_TYPE=="vol":

            h5file = h5py.File("tmp.h5", "w")


            data = h5file.require_dataset("data",
                               shape=( self.VDIMZ  ,self.VDIMY ,self.VDIMX),
                               dtype=np.float32,
                               chunks=(chunkz, chunky, chunkx ),
                               )
            File = open(self.FILE, "r")
            File.seek(0,0)


            if self.FILE_B is not None or self.DERIVATIVES :
                datab = h5file.require_dataset("datab",
                                              shape=( self.VDIMZ  ,self.VDIMY ,self.VDIMX),
                                   dtype=np.float32,
                                   chunks=(chunkz, chunky, chunkx )
                                              )
            else:
                datab = None

            if self.FILE_B is not None :
                FileB = open(self.FILEB, "r")
                FileB.seek(0,0)

            for iz in range(self.VDIMZ):
                if iz%10 ==0 :
                    print  " reading slice ", iz , " over " , self.VDIMZ, "  to transfer it into a hdf5 file "
                vol_data = numpy.reshape(numpy.fromstring( File.read(self.VDIMY*self.VDIMX*4 ), dtype=np.float32), [ self.VDIMY,self.VDIMX   ] )
                # print vol_data.shape

                if self.FILE_B is not None or self.DERIVATIVES :
                    if self.FILE_B is not None :
                        vol_datab = numpy.reshape(numpy.fromstring( FileB.read(self.VDIMY*self.VDIMX*4 ), dtype=np.float32), [ self.VDIMY,self.VDIMX   ] )
                    elif self.DERIVATIVES:
                        X         = scipy.ndimage.filters.sobel(vol_data,axis=1 )/8.0
                        vol_datab = scipy.ndimage.filters.sobel(vol_data,axis=0 )/8.0
                        vol_data  = X


                    # print " per b  " , vol_datab.sum()
                    datab[iz] = vol_datab

                data[iz] = vol_data

            h5file  .close()
            h5file = h5py.File("tmp.h5", "r")
            data = h5file["data"]

            if self.FILE_B is not None or self.DERIVATIVES :
                datab = h5file["datab"]

        else:
            h5file = h5py.File(self.FILE, "r")
            data = h5file["data"]
            if self.FILE_B is not None :
                h5fileb = h5py.File(self.FILE_B, "w")
                datab = h5fileb["data"]
            else:
                datab=None

           

        n_patches = 0
        while n_patches < self.N_PATCHES : 
            coos  = numpy.zeros([self.N_PATCHES, 3],"i")

            marge=0
            if self.DERIVATIVES:
                marge=1


            coos[:,0] = numpy.random.randint(self.START_Z,self.END_Z - dims[0]*self.REBIN  ,self.N_PATCHES )
            coos[:,1] = numpy.random.randint(self.START_Y+marge,self.END_Y - dims[1]*self.REBIN+1-marge ,self.N_PATCHES )
            coos[:,2] = numpy.random.randint(self.START_X+marge,self.END_X - dims[2]*self.REBIN+1-marge ,self.N_PATCHES )
            belongs =   ( (coos[:,0]/chunkz)*(self.VDIMY/chunky) +  coos[:,1]/chunky ) *( self.VDIMX/chunkx )  +     coos[:,2]/chunkx
            iorder = numpy.argsort(belongs)
            coos=coos[iorder,:]
            ip = 0 
            while n_patches < self.N_PATCHES and ip < len(coos): 
                starts = coos[ip]
                ip+=1
                if datab is None:
                    if not  self.DERIVATIVES:
                        if len(data.shape)==3:
                            newpatch = [data[starts[0]:(starts[0]+dims[0]*self.REBIN),starts[1]:(starts[1]+dims[1]*self.REBIN),starts[2]:(starts[2]+dims[2]*self.REBIN)]]
                        else:
                            newpatch = [[data[starts[1]:(starts[1]+dims[1]*self.REBIN),starts[2]:(starts[2]+dims[2]*self.REBIN)]]]

                            newpatch=np.array(newpatch)

                    else:
                        if len(data.shape)==3:
                            item = [data[starts[0]:(starts[0]+dims[0]*self.REBIN),
                                         starts[1]-marge:(starts[1]+dims[1]*self.REBIN)+marge,
                                         starts[2]-marge:(starts[2]+dims[2]*self.REBIN)+marge]]
                            newpatch = [ scipy.ndimage.filters.sobel(item,axis=2 )[:,1:-1,1:-1],
                            scipy.ndimage.filters.sobel(item,axis=1 )[:,1:-1,1:-1]
                            ]
                        else:
                            item = [[data[     starts[1]-marge:(starts[1]+dims[1]*self.REBIN)+marge,
                            starts[2]-marge:(starts[2]+dims[2]*self.REBIN)+marge]]]
                            newpatch = [ scipy.ndimage.filters.sobel(item,axis=2 )[:,1:-1,1:-1],
                                         scipy.ndimage.filters.sobel(item,axis=1 )[:,1:-1,1:-1]
                                         ]
                            
                        
                else:
                    if len(data.shape)==3:
                        newpatch = [ data [ starts[0]:(starts[0]+dims[0]*self.REBIN),   starts[1]:(starts[1]+dims[1]*self.REBIN),   starts[2]:(starts[2]+dims[2]*self.REBIN)] ,
                                     datab[ starts[0]:(starts[0]+dims[0]*self.REBIN),   starts[1]:(starts[1]+dims[1]*self.REBIN),   starts[2]:(starts[2]+dims[2]*self.REBIN)] ]
                        
                    else:
                        newpatch = [[ data [   starts[1]:(starts[1]+dims[1]*self.REBIN),   starts[2]:(starts[2]+dims[2]*self.REBIN)] ,
                                     datab[  starts[1]:(starts[1]+dims[1]*self.REBIN),   starts[2]:(starts[2]+dims[2]*self.REBIN)] ]]
                        
                if self.REBIN>1:
                    for i in range(len(newpatch)):
                        # t=numpy.zeros(dims,"f")
                        # for ri in range(self.REBIN ):
                        #     for rj in range(self.REBIN ):
                        #         t = t + newpatch[i][:,ri::self.REBIN,rj::self.REBIN ]
                        # newpatch[i]=t
                        if len(data.shape)==3:
                            newpatch[i]=newpatch[i][::self.REBIN,::self.REBIN,::self.REBIN ]
                        else:
                            newpatch[i]=newpatch[i][::self.REBIN,::self.REBIN ]


                                
                newpatch=numpy.array(newpatch)
                    
                max_p = newpatch.max()
                min_p = newpatch.min()

                newpatch= numpy.maximum( newpatch ,  self.VAL_MIN) 
                patches_container[n_patches] =  newpatch
                n_patches += 1
                
                # if min_p < self.VAL_MAX and  max_p > self.VAL_MIN:
                #     if n_patches%1000==0:
                #         print " n_patches " , n_patches, "  over " , self.N_PATCHES
                #     patches_container[n_patches] =  newpatch
                #     n_patches += 1
                    
        # os.remove("tmp.h5")
     


if len(sys.argv)==2:

    inputfile=open(sys.argv[1],"r")
    params  = yaml.load(inputfile)

    if params.has_key("patch_eight"):
        patch_height = params["patch_eight"]
    else:
        patch_height = 1 

    if patch_height ==1 :
        patch_shape = (1, params["patchwidth"],params["patchwidth"])
    else:
        patch_shape = (  abs(patch_height),  params["patchwidth"],params["patchwidth"])



    NPURSUIT= params["NPURSUIT"]

    n_comps = params["n_comps"]
    nitersksvd = params["nitersksvd"]

    files = params["files"]

    n_patches_tot = 0
    volume =0.0
    vectoriality=None
    for file in files:
        print "================================== ", file
        n_patches_tot += file.N_PATCHES
        volume += file.volume()
        if file.FILE_B is not None or file.DERIVATIVES:
            if vectoriality is not None:
                assert( vectoriality==2)
            else:
                vectoriality = 2
        else:

            if vectoriality is not None:
                assert( vectoriality==1)
            else:
                vectoriality = 1

    n_patches_tot_old = n_patches_tot
    n_patches_tot =0
    
    for file in files:
        file.N_PATCHES = int( n_patches_tot_old* file.volume()/volume    ) 
        n_patches_tot += file.N_PATCHES


    allpatches = np.zeros([ n_patches_tot , vectoriality, patch_shape[0]]+ list(patch_shape[1:])   ,"f")
    n_patches_tot = 0
    for file in files:
        print " file ", file, patch_shape
        file.get_patches( allpatches[n_patches_tot:] , patch_shape )
        n_patches_tot+=file.N_PATCHES

    S0 = allpatches
    S1 = allpatches[:,:,:,:,::-1]
    S2 = allpatches[:,:,:,::-1,:]
    S3 = allpatches[:,:,:,::-1,::-1]
    S4 = np.swapaxes( allpatches, -1,-2   ) 
    S5 = S4[:,:,:,:,::-1]
    S6 = S4[:,:,:,::-1,:]
    S7 = S4[:,:,:,::-1,::-1]
    

    allpatches = np.concatenate(  [  S0,S1,S2,S3,S4,S5,S6,S7] ).astype("f") 

    print " filtro"
    pL=[]
    for p in allpatches:
        s=p.std()
        t=(p*p).sum()/p.size

        if s>t*1.0e-7:
            pL.append(p)
    origpatches=pL
    patches=pL

    for n_pursuit in range(1,NPURSUIT+1):

        if(n_pursuit>1):
            h5file = h5py.File("patches.h5", "r")
            comps = h5file["data"][:]
            patches= [comp*0.0000000001 for comp in comps]   + origpatches
            h5file.close()
        else:
            patches= [ np.random.random( origpatches[0].shape  )    for i in range(n_comps)]   + origpatches

        patches=np.array(patches)

        print " SHAPE " , patches.shape
        components = c_ksvd.dictionary_learning(patches , n_comps, n_pursuit,nitersksvd )
        components = numpy.reshape( components ,[len(components)] + list(patches.shape[1:])   ) 


        h5file = h5py.File("patches.h5", "w")
        data = h5file.require_dataset("data", shape=components.shape, dtype=np.float32  )
        data[:] = components
        h5file.close()
else:
    print "-- YOU HAVE CALLED %s WITHOUT INPUTFILE ARGUMENT."
    print "-- THIS MEANS THAT YOU WANT TO VISUALIZE A PREVIOUSLY CALCULATED COLLECTION OF PATCHES"
    print "-- please give a h5 filename"
    name=raw_input()
    components = h5py.File(name, "r")["data"][:]


n_comps = len(components)    

nc  = components.shape[0]
ny,nx = components.shape[-2:]

if(len(components.shape)>3):
    ncc = numpy.prod(components.shape[1:-2])
else:
    ncc = 1 

components = np.reshape(components, [nc,ncc, ny,nx]) 


RATIO=1

nlato = int(sqrt(n_comps/1.0/RATIO))
if nlato*nlato*RATIO<n_comps:
    nlato+=1

sncc = int(sqrt(ncc*1.0))
if sncc*sncc < ncc:
    sncc += 1


if visu:
    pl.figure(figsize=(4.2*(nlato/10.0), 4*(nlato/10.0)))

    for i, comp in enumerate(components[:]):
        comp[:]=comp/np.sqrt((comp*comp).sum())
        pl.subplot(nlato, nlato, i + 1)
        tmp = np.zeros( [ sncc*ny, sncc*nx ,  ]    ,"f")
        for i, tt in enumerate( comp   ):
            iy = i/sncc
            ix = i%sncc
            tmp[ iy*ny:(iy+1)*ny  , ix*nx:(ix+1)*nx   ] = tt
        pl.imshow(tmp, cmap=pl.cm.gray_r,
                  interpolation='nearest')
        pl.xticks(())
        pl.yticks(())
    pl.subplots_adjust(0.08, 0.02, 0.92, 0.85, 0.08, 0.23)
    pl.show()
    
