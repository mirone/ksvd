
 
float  tv_denoising_patches_fit(int dimz,int dim0,int dim1,float  *img,float  *imgresult , float weight,
				float *patches, int npatches, int dim_patches, float  *imgB,float  *imgresultB ,
				float rho) ;


 
float  tv_denoising_patches_fit_new(int dimz,int dim0,int dim1,float  *img,float  *imgresult , float weight,
				float *patches, int npatches, int dim_patches, float  *imgB,float  *imgresultB ,
				float rho, int step_for_patches, int maxiter) ;

float  tv_denoising_patches_fit_deco(int dimz,int dim0,int dim1,float  *img,float  *kernel ,float  *imgresult , float weight,
				float *patches, int npatches, int dim_patches, 
				float rho, int step_for_patches, int maxiter) ;

