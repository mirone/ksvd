import cython
import numpy
cimport numpy
import math
 
#### soit on fait comme ca et la partie cdef extern va dans un pxd homonime
#from tv_denoising_gpu  cimport tv_denoising_fista as c_tv_denoising_fista  ## attention aux noms!!
##### ou bien comme ca, en ligne. Dans les deux ca sil faut le .h

cdef extern from "tv_denoising_patches.h":
  cdef void tv_denoising_patches_fit(int    ,
                                    int    ,
                                    int    ,
                                    float *  ,
                                    float * ,
                                    float  ,
                                    float  *,
                                    int    ,
                                     int   ,
                                    float *  ,
                                    float * ,
                                    float   )  nogil
  
  cdef void tv_denoising_patches_fit_new(int    ,
                                    int    ,
                                    int    ,
                                    float *  ,
                                    float * ,
                                    float  ,
                                    float  *,
                                    int    ,
                                     int   ,
                                    float *  ,
                                    float * ,
                                    float,int , int   )  nogil
  
    
  cdef float tv_denoising_patches_fit_deco(int    ,
                                    int    ,
                                    int    ,
                                    float *  ,
                                    float *  ,
                                    float * ,
                                    float  ,
                                    float  *,
                                    int    ,
                                     int   ,
                                    float,int , int   )    nogil
        




@cython.boundscheck(False)
def pytv_denoising_patches(numpy.ndarray input_img not None,
                           float weight,
                           numpy.ndarray input_patches not None,
                           numpy.ndarray input_imgB  ,
                           float rho,
                           int step, int maxiter
                         ):
    """
    """ 
     
    assert input_img.ndim in [3]
    cdef int dimz, dim0,dim1

    dimz = input_img.shape[0]
    dim0 = input_img.shape[1]
    dim1 = input_img.shape[2]

    assert input_patches.ndim in [5]
    cdef int npatches,  dim_patches
    
    npatches    = input_patches.shape[0]

    dim_patches = input_patches.shape[4]

    
    cdef  numpy.ndarray[numpy.float32_t, ndim=1]cinput   = numpy.ascontiguousarray(input_img.ravel(),dtype=numpy.float32)
    cdef  numpy.ndarray[numpy.float32_t, ndim=1]cpatches = numpy.ascontiguousarray(input_patches.ravel(),dtype=numpy.float32)

    print " LE DIMENSIONI SONO " , dimz, dim0,dim1

    cdef  numpy.ndarray[numpy.float32_t, ndim=3]coutput  = numpy.zeros([dimz, dim0,dim1], dtype=numpy.float32)	

    if input_imgB is None:
        iB = numpy.zeros([1,1], dtype=numpy.float32)
        oB_shape = [1, 1,1]
    else:
        iB       =  input_imgB
        oB_shape = [dimz, dim0,dim1]
            
    cdef  numpy.ndarray[numpy.float32_t, ndim=1]cinputB  = numpy.ascontiguousarray( iB.ravel(),dtype=numpy.float32)
    cdef  numpy.ndarray[numpy.float32_t, ndim=3]coutputB = numpy.zeros(  oB_shape , dtype=numpy.float32)	

    if input_imgB is None:

        print " CHIAMO " ,   dimz, dim0, dim1
        with nogil:

            tv_denoising_patches_fit_new( dimz, dim0, dim1,&cinput[0],&coutput[0,0,0],  weight, &cpatches[0], npatches,
                                      dim_patches , NULL,NULL,
                                      rho, step, maxiter)  
	    
        return coutput, None
        
    else:
      with nogil:
          tv_denoising_patches_fit_new(dimz,  dim0,dim1,&cinput[0],&coutput[0,0,0],  weight, &cpatches[0], npatches, 
                                   dim_patches , &cinputB[0],&coutputB[0,0,0],
                                   rho, step, maxiter)  
      return coutput, coutputB



@cython.boundscheck(False)
def pytv_denoising_patches_deco(numpy.ndarray input_img not None,
			   numpy.ndarray kernel    not None,
                           float weight,
                           numpy.ndarray input_patches not None,
                           float rho,
                           int step, int maxiter
                         ):
    """
    """ 
     
    assert input_img.ndim in [3]
    cdef int dimz, dim0,dim1

    dimz    = input_img.shape[0]
    dimdata = input_img.shape[1]
    dim1    = input_img.shape[2]

    assert dimz == 1

    assert kernel.ndim in [2]
    cdef int dimvar
    dimvar    = kernel.shape[1]
    assert dimdata == kernel.shape[0]

    assert input_patches.ndim in [5]
    cdef int npatches,  dim_patches
    
    npatches    = input_patches.shape[0]

    dim_patches = input_patches.shape[4]

    
    cdef  numpy.ndarray[numpy.float32_t, ndim=1]cinput   = numpy.ascontiguousarray(input_img.ravel(),dtype=numpy.float32)
    cdef  numpy.ndarray[numpy.float32_t, ndim=1]cpatches = numpy.ascontiguousarray(input_patches.ravel(),dtype=numpy.float32)
    cdef  numpy.ndarray[numpy.float32_t, ndim=1]ckernel = numpy.ascontiguousarray(kernel.ravel(),dtype=numpy.float32)

    print " LE DIMENSIONI SONO " , dimz, dimdata,dim1

    cdef  numpy.ndarray[numpy.float32_t, ndim=2]coutput  = numpy.zeros([ dimvar,dim1], dtype=numpy.float32)	


    # with nogil:

    error = tv_denoising_patches_fit_deco( dimdata, dimvar,  dim1,&cinput[0],&ckernel[0], &coutput[0,0],  weight, &cpatches[0], npatches,
                                           dim_patches ,
                                           rho, step, maxiter)  
        
    return coutput, error











                   

    
    
