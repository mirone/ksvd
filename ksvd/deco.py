import numpy as np
import h5py 

import scipy.interpolate as interp
from PyMca5.PyMcaIO.EdfFile import EdfFile
# from PyMca.EdfFile import EdfFile


class Parameters:
    check_original=False
    exec(open("input.par","r").read())


def make_interpolated_image():
    print "CREATING AN INTERPOLATED IMAGE"
    h5=h5py.File(Parameters.datafile,"r")
    x0 = h5["data_conv/dt"][:]
    h0 = h5["data_conv/dsq_conv"][:]


    x0 = x0.flatten()
    n0 = np.arange(len(x0)).astype("d")

    f1d=interp.interp1d(n0,x0)    
    
    ovrsmpl =  Parameters.grid_oversampling

    tgrid = f1d(np.arange((len(x0)-1)*ovrsmpl+1.0)/ovrsmpl )

    f1d=interp.interp1d(x0,h0, axis=0)
    
    interp_data = f1d(tgrid)

    h5=h5py.File("interpolated.h5","w")
    h5["data"]=interp_data
    h5.close()
    print "DONE on ", "interpolated.h5"
    


def create_kernel():
    h5=h5py.File(Parameters.datafile,"r")
    x0 = h5["data_conv/dt"][:]




    d=x0[-1]-x0[-2]
    last=x0[-1]

    x0=(np.concatenate([x0,[last+d, last+2*d    ]])).flatten()

    # extension = d+d*np.arange(Parameters.grid_extension)
    # print x0.shape
    # print (last+extension).shape
    # x0=(np.concatenate([x0.flatten(),last+extension])).flatten()

    n0=np.arange(len(x0))

    print n0
    print x0

    f1d=interp.interp1d(n0,x0)    
    
    ovrsmpl =  Parameters.grid_oversampling

    ## this tgrid contains t times of the free variables
    tgrid = f1d(np.arange((len(x0)-1)*ovrsmpl+1.0)/ovrsmpl )
    tgrid=tgrid-tgrid[0]



    data_times = h5["data_conv/dt"][:]

    conv_h = h5["conv_fun/h"][:].flatten()
    conv_t = h5["conv_fun/t"][:].flatten()
    conv_dt = (conv_t[-1]-conv_t[0])/(len(conv_t)-1)

    conv_cumsum=np.zeros(conv_t.shape).astype("d")
    conv_cumsum+=((conv_h.cumsum())/2.0*conv_dt)
    conv_cumsum[1:]+=((conv_h.cumsum())/2.0*conv_dt)[:-1]
    conv_cumsum-=conv_cumsum[0]
 
    conv_cumsumX       = np.zeros(conv_t.shape).astype("d")
    conv_cumsumX += (((conv_h*conv_t).cumsum())/2.0*conv_dt)
    conv_cumsumX[1: ] += (((conv_h*conv_t).cumsum())/2.0*conv_dt)[:-1]
    conv_cumsumX-=conv_cumsumX[0]


    conv_cumsumX[-1]   = conv_cumsumX [-2]
    conv_cumsum [-1]   = conv_cumsum  [-2]
    conv_cumsumX[ 0]   = conv_cumsumX [ 1]
    conv_cumsum [ 0]   = conv_cumsum  [ 1]

    conv_t[ 0]=-1.0e10
    conv_t[-1]=+1.0e10

    conv_f1d  = interp.interp1d( conv_t, conv_cumsum, bounds_error=True, fill_value=0.0)
    convX_f1d = interp.interp1d( conv_t, conv_cumsumX, bounds_error=True, fill_value=0.0)
    



    ## kernel_f: from free variables to data
    kernel_f = np.zeros([ len(data_times), len(tgrid)])

    for idata in range(len(data_times)):
        for ivari in range(len(tgrid)):
            if ivari>0:

                a=( tgrid[ivari-1]  + Parameters.t0 )- data_times[idata]  
                b=( tgrid[ivari  ]  + Parameters.t0 ) -data_times[idata]  
                    
                if 0:
                ## The integration for the segment going from ivari-1  to ivari
                ## The function is assumed to be 0 in ivari-1 and 1 in ivari
                ## Taking the origin ot the coordinates in data_times[idata]
                ## the free function is zero in a ( see below: it is the relative time of ivari-1)
                ## and is 1 in b (ivari).
                ## The free function is then   (x-a)/(b-a). The integrand is fconv(-x)  (x-a)/(b-a)
                ## The result that we search is therefore:
                ##                Integral_(a2b)      fconv(-x)  (x-a)/(b-a)=
                ##                Integral_(-a2-b)      fconv(y)  (y+a)/(b-a)=
                ##
                ##         (convX_f1d(y)/(b-a)+conv_f1d(y)*a/(b-a))|_(-a,-b)
                ##
                ##
                ## t0 in inglobated into the kernel
                    
                    
                    kernel_f[idata,ivari] += ( conv_f1d(-b)  -    conv_f1d(-a)  )*a/(b-a)
                    kernel_f[idata,ivari] += ( convX_f1d(-b) -   convX_f1d(-a)  )  /(b-a)
                    
                else:
                    
                    
                ## The free function is then   (x-a)/(b-a). The integrand is fconv(x)  (x-a)/(b-a)
                ## The result that we search is therefore:
                ##                Integral_(a2b)      fconv(x)  (x-a)/(b-a)=
                ##
                ##         (convX_f1d(x)/(b-a)-conv_f1d(y)*a/(b-a))|_(a,b)
                ##
                ##
                ## t0 in inglobated into the kernel
                    
                                        
                    kernel_f[idata,ivari] -= ( conv_f1d(b)  -    conv_f1d(a)  )*a/(b-a)
                    kernel_f[idata,ivari] += ( convX_f1d(b) -   convX_f1d(a)  )  /(b-a)
                    

            if ivari<len(tgrid)-1:


                b=tgrid[ivari+1] + Parameters.t0 - data_times[idata]
                a=tgrid[ivari]   + Parameters.t0 - data_times[idata]


                if 0:

                ## The integration for the segment going from ivari  to ivari+1
                ## The function is assumed to be 1 in ivari  and 0 in ivari+1 
                ## Taking the origin of the coordinates in data_times[idata]
                ## the free function is zero in b ( see below: it is the relative time of ivari+1)
                ## and is 1 in a (ivari).
                ## The free function is then   (b-x)/(b-a). The integrand is fconv(-x)  (b-x)/(b-a)
                ## The result that we search is therefore:
                ##                Integral_(a2b)      fconv(-x)  (b-x)/(b-a)=
                ##                Integral_((-b)2(-a))    fconv(y)  (b+y)/(b-a)=
                ##
                ##         (convX_f1d(y)/(b-a)+conv_f1d(y)*b/(b-a))|_(-b,-a)
                ##
                ##
                ## t0 in inglobated into the kernel

                    
                    kernel_f[idata,ivari] += ( conv_f1d(-a)  -    conv_f1d(-b)  )*b/(b-a)
                    kernel_f[idata,ivari] += ( convX_f1d(-a) -   convX_f1d(-b)  )  /(b-a)
                    
                else:
                ## The free function is then   (b-x)/(b-a). The integrand is fconv(x)  (b-x)/(b-a)
                ## The result that we search is therefore:
                ##                Integral_(a2b)      fconv(x)  (b-x)/(b-a)=
                ##
                ##         (-convX_f1d(x)/(b-a)+conv_f1d(x)*b/(b-a))|_(a,b)
                ##
                ##
                    
                    kernel_f[idata,ivari] += ( conv_f1d(b) -    conv_f1d (a)  )*b/(b-a)
                    kernel_f[idata,ivari] -= ( convX_f1d(b) -   convX_f1d(a)  )  /(b-a)
                    


    return kernel_f, tgrid
                

def main():


    if Parameters.interpolate_data:
        """ do this if you want to synthetize an image with a finer time resolution."""
        make_interpolated_image()
    elif Parameters.check_original:
        do_convolution()
        
    else:
        do_deconvolution()



        original_scaled = get_scaled_original()

def do_convolution():

    kernel, times_grid  = create_kernel()
    
    original = get_interpolated_original(times_grid)

    h5=h5py.File(Parameters.datafile,"r")
    target  = h5["/data_conv/dsq_conv"][:]
    h5.close()

    convoluted = np.dot( kernel, original )

    h5=h5py.File("comparison.h5","w")
    h5["original"] = original
    h5["convolution"] = convoluted
    h5["target"] = target
    h5["error"]= target-convoluted
    h5.close()
    
    
def get_interpolated_original(times_grid):

    h5=h5py.File(Parameters.datafile,"r")
    times = h5["data_init/time"][:].flatten()
    qs    = h5["data_init/q"]   [:].flatten()
    h0    = h5["data_init/dsq"] [:]
    h5.close()

    f2d=interp.interp2d(qs, times, h0)
  
    return f2d(qs,times_grid)






def do_deconvolution():

    kernel, times_grid  = create_kernel()

    import getgpu
    gpu=getgpu.getgpu()
    try:
        import pycuda.autoinit
        import pycuda.tools
        if gpu!=-1:
            dev=pycuda.driver.Device(gpu)
            CTX=dev.make_context()
        else:
            CTX=pycuda.tools.make_default_context() 
        print "Ma carte est ", pycuda.driver.Context.get_device().name()
    except:
        print " NO pycuda "
        # raise " pas de carte et/ou cuda "


    import tv_denoising_patches

    beta  = Parameters.beta
    rho  = Parameters.rho
    patches_file = Parameters.patches_file

    h5 = h5py.File( patches_file ,"r")
    patches = h5["data"][:]

    print " SHAPE PATCHES " , patches.shape

    if len(patches.shape)==4:
        epaisseur = 1
        patches.shape = list(patches.shape[:2])+[1] + list(patches.shape[2:])

    assert(len(patches.shape)==5)

    h5=h5py.File(Parameters.datafile,"r")
    image = np.array([h5["data_conv/dsq_conv"][:]])


    pp  =  np.ones(patches.shape[1:],"f")
    pp[:] =  np.sqrt(1.0/pp.size)
    patches= np.concatenate( ([pp],patches), axis=0)

 
    # l asse di convoluzione est 0
    res, error = tv_denoising_patches.pytv_denoising_patches_deco(image, kernel, beta, np.array(patches,"f"),  rho, 2, 2000)

    CTX.detach()
    print res.shape
    # res.shape=[res.shape[1], res.shape[2]]


    h5=h5py.File("res.h5","w")
    h5["data"] = res
    h5["times"]= times_grid
    h5.close()
    print "DONE on ", "interpolated.h5"

    print " ERROR " , error

main()
    
